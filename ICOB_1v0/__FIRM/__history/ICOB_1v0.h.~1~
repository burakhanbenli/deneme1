#device ADC=8
#include <33EP256MU806.h>
#device ICSP=1
#use delay(clock=80MHz,crystal=16MHz, USB_FULL, AUX:clock=48MHz)

#FUSES NOWDT                 	//No Watch Dog Timer
#FUSES NOWRT                 	//Program memory not write protected
#FUSES NOPROTECT             	//Code not protected from reading
#FUSES GSSK                  	//General Segment Key bits, use if using either WRT or PROTECT fuses
#FUSES IESO                  	//Internal External Switch Over mode enabled
#FUSES NOOSCIO               	//OSC2 is clock output
#FUSES NOIOL1WAY             	//Allows multiple reconfigurations of peripheral pins
#FUSES CKSFSM                	//Clock Switching is enabled, fail Safe clock monitor is enabled
#FUSES PLLWAIT               	//Clock switch to PLL will wait until the PLL lock signal is valid
#FUSES WINDIS                	//Watch Dog Timer in non-Window mode
#FUSES NOPUT                 	//No Power Up Timer
#FUSES NOBROWNOUT            	//No brownout reset
#FUSES ALTI2C1               	//I2C1 mapped to ASDA1/ASCL1 pins
#FUSES RESET_PRIMARY         	//Device will reset to Primary Flash Reset location
#FUSES NOJTAG                	//JTAG disabled
#FUSES NOAWRT                	//Auxiliary program memory is not write-protected
#FUSES NOAPROTECT            	//Auxiliary program memory is not code-protected
#FUSES APLK                  	//Auxiliary Segment Key bits, use if using either AWRT or APROTECT fuses


#use FIXED_IO( B_outputs=PIN_B15,PIN_B13,PIN_B12,PIN_B10,PIN_B9,PIN_B8,PIN_B7,PIN_B6,PIN_B5,PIN_B4,PIN_B3 )
#use FIXED_IO( C_outputs=PIN_C14,PIN_C13 )
#use FIXED_IO( E_outputs=PIN_E5,PIN_E4,PIN_E3,PIN_E2,PIN_E0 )
#use FIXED_IO( D_outputs=PIN_D11,PIN_D10,PIN_D9,PIN_D6,PIN_D5,PIN_D3,PIN_D1 )
#use FIXED_IO( F_outputs=PIN_F5,PIN_F1 )
#use FIXED_IO( G_outputs=PIN_G9,PIN_G8,PIN_G6,PIN_G2 )

#define VBAT	PIN_B0
#define INT2/IGN_CNT	PIN_B1
#define Vsys	PIN_B2
#define IGNOUT_CHECK	PIN_B3
#define WIRE_CHECK	PIN_B4
#define HORN	PIN_B5
#define PWR_EN	PIN_B8
#define 5V_EN	PIN_B9
#define CAM1	PIN_B10
#define INT1/ACC_CNT	PIN_B11
#define CHARGE	PIN_B12
#define ALC1	PIN_B13
#define Vxyz	PIN_B14
#define CS2	PIN_C14
#define ATTACH_CNT	PIN_E2
#define BUZZER	PIN_E3
#define LAMP	PIN_E4
#define 12V_HEATER_EN	PIN_E5
#define CHG_ST	PIN_E6
#define INT3/PWR_CNT	PIN_E7
#define INT0/RTC_INT	PIN_D0
#define CS1	PIN_D3
#define WIRE_ST	PIN_D8
#define SDA	PIN_D9
#define SCL	PIN_D10
#define WIFI_EN	PIN_D11
#define ALC_ATTACH	PIN_F0
#define WIFI_RST	PIN_F1
#define MOVEMENT	PIN_F3
#define SCK	PIN_G6
#define SDI	PIN_G7
#define SDO	PIN_G8
#define IGN	PIN_G9

#pin_select U1TX=PIN_D5
#pin_select U1RX=PIN_D4
#use rs232(UART1, baud=9600, errors, stream=GSM)

#pin_select U2TX=PIN_D6
#pin_select U2RX=PIN_D7
#use rs232(UART2, baud=115200, errors, stream=WIFI)

#pin_select U3TX=PIN_F5
#pin_select U3RX=PIN_F4
#use rs232(UART3, baud=9600, errors, stream=GPS)

#pin_select SCK1OUT=PIN_G6
#pin_select SDI1=PIN_G7
#pin_select SDO1=PIN_G8
#use spi(MASTER, SPI1, BAUD=115200, MODE=0, BITS=8, stream=SPI_PORT1)

#use i2c(MASTER, I2C1, FAST, stream=I2C_PORT1)


#define USB_CONFIG_VID 0x1111
#define USB_CONFIG_PID 0x1112
#define USB_CONFIG_BUS_POWER 500
#define USB_STRINGS_OVERWRITTEN

char USB_STRING_DESC_OFFSET[]={0,4,16};

char const USB_STRING_DESC[]={
   //string 0 - language
      4,  //length of string index
      0x03,  //descriptor type (STRING)
      0x09,0x04,  //Microsoft Defined for US-English
   //string 1 - manufacturer
      12,  //length of string index
      0x03,  //descriptor type (STRING)
      'A',0,
      'R',0,
      'M',0,
      'A',0,
      'S',0,
   //string 2 - product
      10,  //length of string index
      0x03,  //descriptor type (STRING)
      'I',0,
      'C',0,
      'O',0,
      'B',0
};

#define USB_CONFIG_HID_TX_SIZE 63
#define USB_CONFIG_HID_RX_SIZE 63
#include <pic24_usb.h>
#include <usb_desc_hid.h>
#include <usb.c>
#build(stack=1024)

