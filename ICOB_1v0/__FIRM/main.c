#INT_CAN1
void can1_isr(void)
{
   // TODO: add CAN receive code here
}

//!#INT_CAN2// CAR_CAN
//!void can2_isr(void)
//!{
//!   // TODO: add CAN receive code here
//!}

#INT_EXT0// INT0/RTC_INT -
void  ext0_isr(void) 
{
   disable_interrupts(INT_EXT2);

   if(lock==1){lock=0; break;}
   
   if(alarmst==0 && retest==1 && acc_mode==1) minute++;
   
   if((alarmst==1) && (acc_mode==1))
   {
      retest=1;
      wait_retest=0;
      test_st=1;
   }
   
   if((retest) && (acc_mode==0))
   {
      minute++;
   }
   
   if(alarmst==2)
   {
      minute++;
      if(minute==2)
      {
         minute=0;
         alarmst=3;
      }
   }
   
   enable_interrupts(INT_EXT2);
}

#INT_RDA// GSM -
void  rda_isr(void) 
{
   disable_interrupts(INT_RDA2);
   
   bt_data=getch(GSM);
   if(!gsm_data_geldi)
   {
      sayac=0; data_counter=0;
      sim_data[0][0]=bt_data; sim_data[0][1]=0;
      gsm_data_geldi=1;
      do{
         sayac++;
         if(kbhit(GSM))
         {
            receive_data=getch(GSM);
            data_counter++;
            sim_data[data_counter][0]=receive_data;
            sim_data[data_counter][1]=NULL;
         }
         if(sayac>65500) break;
      }while(data_counter<100);
   }
   enable_interrupts(INT_RDA2);
}

#INT_CNI// MOVEMENT(ACCELOREMETER)
void  cni_isr(void) 
{

}

#INT_EXT1// INT1/ACC_CNT -
void  ext1_isr(void) 
{
   disable_interrupts(INT_EXT1);
   
   if(acc_mode==0)
   {
      acc_mode=1;
      ext_int_edge(1, L_TO_H);
   }
   else if(acc_mode==1)
   {
      acc_mode=0;
      ext_int_edge(1, H_TO_L);
   }
   
   enable_interrupts(INT_EXT1);
}

#INT_EXT2// INT2/IGN_CNT
void  ext2_isr(void) 
{

}

#INT_RDA2// WIFI -
void  rda2_isr(void) 
{
   bt_data = getch(WIFI);
   if (!wifi_data_geldi)
   {
      disable_interrupts(INT_RDA);
      sayac=0;data_cntr=0;
      wifi_data[0][0] = bt_data; wifi_data[0][1] = 0;
      wifi_data_geldi=1;
       do
       {
           sayac++;
           if (kbhit(WIFI))
           {
               receive_data = getch(WIFI);
               //if(receive_data=='A') data_cntr=1;
               data_cntr++;
               wifi_data[data_cntr][0] = receive_data;
               wifi_data[data_cntr][1] = NULL;
           }
           if (sayac > 65500) break;
       }while(data_cntr < 100);
      enable_interrupts(INT_RDA);
   }
}

#INT_C1RX
void  c1rx_isr(void) 
{
   if(can_kbhit())
   {
      if(can_getd(rx_id, &can_buffer, rx_len, rxstat))
      {
         can_data=1;
      }
   }
}

//!#INT_C2RX
//!void  c2rx_isr(void) 
//!{
//!
//!}

#INT_EXT3// INT3/PWR_CNT
void  ext3_isr(void) 
{

}

#INT_EXT4// INT4/ALC_ATTACH
void  ext4_isr(void) 
{

}

#INT_RDA3// GPS -
void  rda3_isr(void) 
{
      gps_data=getc(GPS);
   
   if(gps_counter==7)
   {
      if((gps_array[4]=='G') && (gps_array[5]=='A') && (gps_array[6]==','))
      {
         get_gps_data=1;
         gps_done=0;
      }
      else
      {
         get_gps_data=0;
         gps_counter=0;
         gps_done=1;
      }
   }
   
   switch(gps_data)
   {
      case '$':
         gps_counter=0;
         get_gps_data=1;
         gps_array[4]='0'; gps_array[5]='0'; gps_array[6]='0';
      break;
   }
   
   if(get_gps_data)
      gps_array[gps_counter++]=gps_data;
}

void main()
{
   init();
   
   while(TRUE)
   {
      
      while(acc_mode==0)
      {
         output_low(IGN);
         output_low(HORN);
         
         if(wait_retest==1)
         {
            /*tekrar testi istenmeden ara� kapat�lm��sa;(wait_restest=1)
            5 dk l�k bir geri say�m koy e�er bu s�re i�inde tekrar �al��t�r�lmazsa sleepe git
            e�er tekrar �al��t�r�l�rsa alarm� beklemeye devam et*//////OK!
            for(int i=0;i<301;i++)
            {
               delay_ms(995);
//!               wiring_check();
//!               ign_check();
               if(acc_mode==1)
               {
                  output_high(IGN);
                  output_high(READY_FOR_IGN);
                  if(!input(INT2_IGN_CNT))
                  {
                     delay_ms(10000);
                     stall_time_delay=1;
                     output_low(IGN);
                     output_low(READY_FOR_IGN);
                     goto LABEL2;
                  } 
               }
               else if(!acc_mode)
               {
                  output_low(IGN);
                  output_low(READY_FOR_IGN);
               }
            }
            output_low(IGN);
            output_low(READY_FOR_IGN);
            wait_retest=0;
            
            if(acc_mode==1)   goto LABEL2;
         }
         else if(retest==1)
         {
            /*tekrar testi istenmi� ve ara� kapat�lm��sa;
            bir s�re sesli ve g�r�nt�l� ikaz vererek test yap�lmas�n� iste
            testin yap�l�p yap�lmad���n� yap�ld�ysa sonu�lar� kay�t alt�na al;*////OK!!????? OK DE��L BUNU NE ���N KOYMU�UM B�LM�YORUM!! :D
            goto LABEL2;
         }
         
         //wiring_check();
         // if(wiring_ok==0)  kablo ba�lant�s�nda m�dahale var!! 
         // else              yap�lmak istenen ne?
         
         //ign_check();
         //if(ign_on==1 & ign_perm==0)    motora izinsiz ign verildi!!
         //else                           yap�lmak istenen ne?

         //setsleep();         
      }
      
      while(acc_mode)
      {
         LABEL2:
         battery_check();
         //ignition_test();
      }
   }
}
void init()
{
   set_tris_b(0xC807);
   set_tris_c(0x0009);
   set_tris_d(0x0195);
   set_tris_e(0x00c2);
   set_tris_f(0x001d);
   set_tris_g(0x0088);
   
   output_b(0x0000);
   output_c(0x0000);
   output_d(0x0000);
   output_e(0x0000);
   output_f(0x0000);
   output_g(0x0000);
   
   
   setup_adc_ports(sAN0 | sAN2 | sAN14, VSS_VDD);
   setup_adc(ADC_OFF | ADC_TAD_MUL_0);

   can_init();

   //can2_init();

   can_disable_interrupts(RB);
   disable_interrupts(INT_CAN1);

//!   can2_disable_interrupts(RB);
//!   disable_interrupts(INT_CAN2);

   disable_interrupts(INT_EXT0);
   disable_interrupts(INT_EXT1);
   disable_interrupts(INT_EXT2);
   disable_interrupts(INT_EXT3);
   disable_interrupts(INT_EXT4);

   disable_interrupts(INT_CNI);
   
   disable_interrupts(INT_RDA);
   disable_interrupts(INT_RDA2);
   disable_interrupts(INT_RDA3);
   disable_interrupts(INT_RDA4);
   
   disable_interrupts(INT_C1RX);
//!   enable_interrupts(INT_C2RX);

   disable_interrupts(INTR_GLOBAL);
   
   output_high(PWR_EN);
   output_low(EN_5V);
   
   output_high(BUZZER);
   delay_ms(500);
   output_low(BUZZER);
   delay_ms(500);
   
   vin_lvl_det();
   
   if(!input(INTT1_ACC_CNT)) acc_mode=1;
   else acc_mode=0;
   
   ext_int_edge( 1, H_TO_L);//acc
   enable_interrupts(GLOBAL);
   enable_interrupts(INT_EXT1);
   
   
   usb_init();
   
   if(dummy==1){}
}

void setsleep()
{
//!   output_high(PWR_EN);
//!   output_low(pwr_5v);
//!   
//!   setup_uart(0, P0);
//!   setup_uart(0, P1);
//!   
//!   set_tris_a(0x050f);
//!   set_tris_b(0x0084);
//!   set_tris_c(0x0000);
//!   
//!   output_c(0x0000);
//!   output_b(0x0000);
//!   output_a(0x0000);

   test_st=0;
   
   if(input(INTT1_ACC_CNT)){acc_mode=0; ext_int_edge(1, H_TO_L);}
   else {acc_mode=1; ext_int_edge(1, L_TO_H);}
//!   pin_select("INT2",Rtc_CLK,TRUE,FALSE);
//!   int1st=clk;
//!   ext_int_edge(2, L_TO_H);     //INT1 H to L
  
   if(lock==1)
   {
      //setAlarm(PER_HOUR);///////
      enable_interrupts(INT_EXT0);
      disable_interrupts(INT_EXT1);
      enable_interrupts(INT_EXT2);
   }
   else 
   {
//!      battery_check();
      //setAlarm(PER_HOUR);///////
      enable_interrupts(INT_EXT0);
      enable_interrupts(INT_EXT1);      
      enable_interrupts(INT_EXT2);
      enable_interrupts(INT_EXT3);
      enable_interrupts(INT_EXT4);
      disable_interrupts(INT_RDA);
      disable_interrupts(INT_RDA2);
   }
   sound_buzzer(400,150,1);

//!   PMD2 = 0xFFFF;
//!   PMD3 = 0xFFFF;
//!   
//!   U2EN = 0;
//!   U1EN = 0;
//!   set_uart_speed(0,P0);
//!   set_uart_speed(0,P1);
//!   
//!   output_c(0x0000);
//!   output_b(0x0400);
//!   output_a(0x0000);
   
//!   output_low(horn_lamp_12V);
//!   output_low(horn_lamp_24V);
//!   output_high(PWR_EN);
//!   output_low(pwr_5v);
   
   sleep(SLEEP_FULL);

   
//!   U1EN=1;  
//!   U1WAKE=1;
//!   //U1RXIE=1; 
//!   INT0EP = 0;
//!   INT1EP = 1;
//!   INT2EP = 0;
//!   INT1EN=1;
//!   INT2EN=1;

   //reset_cpu();
   
//!   PMD1 = 0x0000;
//!   PMD2 = 0x0000;
//!   PMD3 = 0x0000;
   
}

void sound_buzzer(int16 ms, int16 sq_us, keytonee)
{
   if(keytonee)
   {
      //!//sq_us>500 olmal�
      if(sq_us==100)  sq_us=120; 
      if(ms==10)  ms=15; 
      
      int16 tmp_16;
      int16 tmp_uto_ms=(sq_us*2)/100;
      int16 tmp=ms/(tmp_uto_ms)*10;
   
      for (tmp_16=0;tmp_16<tmp;tmp_16++)
      {
         //if (ses_ikz) 
         output_high(BUZZER);
         delay_us(sq_us);
         output_low(BUZZER);
         delay_us(sq_us);
      }
   }
}

//?
//!////////// batarya seviyesi kontrol edilerek �arj durumuna karar veriliyor...
void battery_check()
{
   setup_adc(ADC_CLOCK_INTERNAL);
   set_adc_channel(0);
   delay_us(100);

   for(int i=0;i<20;i++)
   {
      bat_val = read_adc(ADC_START_AND_READ);
      delay_us(100);
      bat_val_total = bat_val_total + bat_val;
      if(bat_val<=590)
         bat_val_cntr++;
   }
   bat_val=bat_val_total/20;
   
   if(bat_val_cntr<12)
   {
      bat_low=0;
      output_low(CHARGE);
      //sound_buzzer(600,150,1);
      delay_ms(500);
//!      if(!acc_mod_on)
//!      {
//!         pin_select("INT2",Rtc_CLK,TRUE,FALSE);
//!         int2st=clk;
//!         INT2EP = 0; //INT2 L to H  
//!      }
   }
   else
   {
      bat_low=1;
      output_high(CHARGE);
      //sound_buzzer(200,150,1);
      delay_ms(500);
//!      if(!acc_mod_on)
//!      {
//!         pin_select("INT2",chng_st,TRUE,FALSE);
//!         int2st=chg;
//!         INT2EP = 0; //INT2 L to H  +
//!      }
   }
   setup_adc(ADC_OFF);
   bat_val_cntr=0;
   bat_val_total=0;
}

////////// giri�deki gerilim seviyesi kontrol edilerek gerekli atamalar yap�l�yor...
int vin_lvl_det()
{
   setup_adc(ADC_CLOCK_INTERNAL);
   set_adc_channel(2);
   delay_us(100);
   vin_sys = read_adc(ADC_START_AND_READ);
   delay_us(100);

   if(vin_sys>465)// 1,5V dan b�y�kse 24V giri�
   {
      vin_lvl=1; 
   }
   else if(155<vin_sys<=465)// 1,5V dan k���kse 12V giri�
   {
      vin_lvl=0; 
   }
   else 
      low_pwr=1;
      
   setup_adc(ADC_OFF);
   
   return vin;
}

////////////////////////////
/// 731 1g kuvvete e�it(yer �ekimi)
/// artan her 0.1g i�in factor de�i�keni 20 artar
/// 
////////////////////////////
void read_acceleration()
{
   unsigned int16 factor=0;
   
   setup_adc(ADC_CLOCK_INTERNAL);
   set_adc_channel(14);
   delay_us(100);
   factor = read_adc(ADC_START_AND_READ);
   delay_us(100);
   
   factor = factor - 731;
   if(factor>20)
   {
      factor = factor / 20;
      accel = accel + factor * 0.1;
   }
   else
      accel = 1;  

   setup_adc(ADC_OFF);
}
