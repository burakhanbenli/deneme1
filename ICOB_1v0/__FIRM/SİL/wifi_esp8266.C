
int1 check_OK()///
{
   int1 return_ok=0;
   if(wifi_data_geldi)
   {
      wifi_data_geldi=0;
      for(int8 i=1;i<100;i++)
      {
         if((wifi_data[i-1][0]=='O') && (wifi_data[i][0]=='K'))
         {
            return_ok=1;
            break;
         }
      }
   }
   return return_ok;
}

void find_linkID()///Donus yapilan hattin ID si aliniyor. Bu ID kullanilarak geri donus yapilir.
{
   linkID="";
   if(wifi_data_geldi)
   {
      wifi_data_geldi=0;
      for(int8 i=0;i<95;i++)
      {
         if((wifi_data[i][0]=='+') && (wifi_data[i+1][0]=='I') && (wifi_data[i+2][0]=='P') && (wifi_data[i+3][0]=='D'))
            {
               sprintf(linkID,"%c",wifi_data[i+5][0]);
            break;
            }
      }
   }
}

void reset_wifi()///
{
   fprintf(WIFI,"%s","AT+RST\r\n");
   delay_ms(1000);
}

int1 open_wifi(char *wifi_ID, char *wifi_pwd)///Bir WIFI agi kurmak icin kullaniliyor.
{
   fprintf(WIFI,"%s","AT\r\n");
   if(!check_OK()) return False;   
   fprintf(WIFI,"%s","AT+CWSAP_CUR=\"");
   fprintf(WIFI,"%s",wifi_ID);
   fprintf(WIFI,"%s","\",\"");
   fprintf(WIFI,"%s",wifi_pwd);
   fprintf(WIFI,"%s","\",5,0\r\n");
   if(!check_OK()) return False;   
   return True;
}

int1 create_server()///WIFI agi uzerinde server kurmak icin kullaniliyor.//80 portunda
{
   fprintf(WIFI,"%s","AT+CWDHCP_CUR=2,1\r\n"); //0,1
   if(!check_OK()) return False;   
   fprintf(WIFI,"%s","AT+CWMODE=3\r\n");
   if(!check_OK()) return False;   
   fprintf(WIFI,"%s","AT+CIPMUX=1\r\n");
   if(!check_OK()) return False;
   fprintf(WIFI,"%s","AT+CIPSERVER=1,80\r\n"); //0,1
   if(!check_OK()) return False;
   fprintf(WIFI,"%s","AT+CIPSTO=600\r\n");
   if(!check_OK()) return False;
   return True;
}

int1 connect_wifi(char *wifi_ID, char *wifi_pwd)///Bir WIFI agina baglanmak icin kullaniliyor.
{
   fprintf(WIFI,"%s","AT\r\n");
   if(!check_OK()) return False;   
   fprintf(WIFI,"%s","AT+CWJAP=\"");
   fprintf(WIFI,"%s",wifi_ID);
   fprintf(WIFI,"%s","\",\"");
   fprintf(WIFI,"%s",wifi_pwd);
   fprintf(WIFI,"%s","\",5,0\r\n");
   if(!check_OK()) return False;   
   return True;
}

int1 send_data(int32 *len)
{
   fprintf(WIFI,"%s","AT+CIPSEND=");
   fprintf(WIFI,"%s",linkID);
   fprintf(WIFI,"%s",",");
   fprintf(WIFI,"%s",len);
   fprintf(WIFI,"%s","\r\n");
   if(!check_OK()) return False;
   for(int i=0;i<len;i++)
      fprintf(WIFI,"%s",wifi_data[i]);
   return True;
}

int1 set_APIP_as(char *APIP)///AP IP setleme
{
   fprintf(WIFI,"%s","AT+CIPAP_CUR=\"");
   fprintf(WIFI,"%s",APIP);
   fprintf(WIFI,"%s","\"\r\n");
   if(!check_OK()) return False;   
   return True;
}

int1 set_STAIP_as(char *STAIP)///STA IP setleme
{
   fprintf(WIFI,"%s","AT+CIPSTA_CUR=\"");
   fprintf(WIFI,"%s",STAIP);
   fprintf(WIFI,"%s","\"\r\n");
   if(!check_OK()) return False;   
   return True;
}























