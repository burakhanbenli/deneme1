
#INT_EXT0
void  ext0_isr(void) 
{

}

#INT_RDA
void  rda_isr(void) 
{

}

#INT_EXT1
void  ext1_isr(void) 
{

}

#INT_EXT2
void  ext2_isr(void) 
{

}

#INT_RDA2
void  rda2_isr(void) 
{

}

#INT_C1RX
void  c1rx_isr(void) 
{

}

#INT_EXT3
void  ext3_isr(void) 
{

}

#INT_EXT4
void  ext4_isr(void) 
{

}

#INT_C2RX
void  c2rx_isr(void) 
{

}

#INT_RDA3
void  rda3_isr(void) 
{

}



void main()
{


   setup_adc_ports(sAN11, VSS_VDD);
   setup_adc(ADC_OFF | ADC_TAD_MUL_0);

   can_init();

   can2_init();

   usb_init();
   disable_interrupts(INT_EXT0);
   disable_interrupts(INT_RDA);
   disable_interrupts(INT_EXT1);
   disable_interrupts(INT_EXT2);
   disable_interrupts(INT_RDA2);
   disable_interrupts(INT_C1RX);
   disable_interrupts(INT_EXT3);
   disable_interrupts(INT_EXT4);
   disable_interrupts(INT_C2RX);
   disable_interrupts(INT_RDA3);
   disable_interrupts(INTR_GLOBAL);

   while(TRUE)
   {
      //TODO: User Code
   }

}
