//!void gps_data_expand()   /// k�sa datan�n ilk 4 indexi al�n�yor sonraki 3 indexin int kar��l��� bulunup 2 ascii karakter elde edilerek yaz�l�yor
//!{                            /// sonraki 6 index direk al�n�yor son 3 indexin int kar��l��� bulunup 2 ascii karakter elde edilerek yaz�l�yor        
//!   int8 value;
//!   char dnm[3]="";
//!   s_gpsdata_expand="";
//!   
//!   for(int j=0;j<16;j++)
//!   {
//!      dnm="";
//!      if( ((7>j) && (j>3)) || j>12)
//!      {
//!         value=loc_inf[j];
//!         sprintf(dnm,"%02d",value); strncat(s_gpsdata_expand,dnm,2);
//!      }
//!      else
//!      {
//!         dnm[0]=loc_inf[j];
//!         strncat(s_gpsdata_expand,dnm,1);
//!      }
//!   }
//!}

int1 GPS_satellite_info()
{
   unsigned char glnbyte00[3];
   char nmbr_of_sat_in_use[8];
   
   if(gps_array[datastart+27]==49 || gps_array[datastart+27]==50)
   {
      if( ((47<gps_array[datastart])&&(gps_array[datastart]<58)) && ((47<gps_array[datastart+13])&&(gps_array[datastart+13]<58)) && 
          ((50<gps_array[datastart+30])&&(gps_array[datastart+30]<58)) && ((gps_array[datastart+11]=='N') || (gps_array[datastart+11]=='S')))
      {
         itoa(gps_array[datastart+27],10,glnbyte00);  
         sprintf(nmbr_of_sat_in_use,"NS:%c%c",gps_array[datastart+29],gps_array[datastart+30]);    // Ka� tane uydunun kullan�ld���n� i�eren datalar (04,08,... vs)         
         gps_array[datastart+27]='0';//bir sonraki test i�in s�f�rla
         return(1);
      }
      else
      {
         gps_array[datastart+27]='0';//bir sonraki test i�in s�f�rla
         return(0);
      }
   }
    else return(0);
}

void GPS_location_calc()
{
   char long_float[30],long_dnm[10],lat_float[30],lat_dnm[10],dnm[10],space1[2]=" ";
   float lat_float_conv,long_float_conv;
   int8 sign,dot=46;
   loc_inf="";
   unsigned int8 lat;
   unsigned int8 log;
   
   /////////////////////////////////// data format�:
   // �rnek konum: +39.123456 +032.987654
   // data: [+][3][9][.][12][34][56][ ][+][0][3][2][.][98][76][54]  16 byte uzunlu�unda 
   // s�ras� ile enlem ve boylam bilgileri [ ](space) ile ayr�lm�� durumda
   // enlem ve boylam�n noktaya kadar olan k�sm� direk ascii olarak kaydedildi
   // [.] dan sonraki 6 hane ikili olarak guruplanarak int8 e d�n��t�r�ld� ve bu int8 say�n�n ascii kar��l��� kaydedildi
   
   /////////////////////////////////// latitude hesaplanan k�s�m
   
   //gps_array[30]="3",gps_array[31]="2",gps_array[33]="1",gps_array[34]="5",gps_array[35]="0",gps_array[36]="8",gps_array[37]="4",gps_array[38]="3",gps_array[39]="7";
   //gps_array[41]="N";
   
   if(gps_array[datastart+11]=='S') // south ya da north olma durumuna g�re koordinat�n ba��na eksi eklendi
      sign=45; // -
   else 
      sign=43; // +
   
   sprintf(latitude,"%c%c%c%c",sign,gps_array[datastart],gps_array[datastart+1],dot); //enlem bilgisinin desimal k�sm�n� veren 2 digit birle�tirilerek tek bir say� elde edildi ( 53. gibi)
   
   sprintf(lat_float,"%c%c.%c%c%c%c%c",gps_array[datastart+2],gps_array[datastart+3],gps_array[datastart+5],                           //enlem bilgisinin virg�lden sonras�n� veren 7 digit
                                       gps_array[datastart+6],gps_array[datastart+7],gps_array[datastart+8],gps_array[datastart+9]);   //birle�tirilerek tek bir say� elde edildi ( 1,2,3,4,5,6,7 -> 12.34567 gibi)
                                                                                                                                             
   lat_float_conv=atof(lat_float);  // birle�tirilerek olu�turulan virg�ll� say� float format�na d�n��t�r�ld�
   
   lat_float_conv=lat_float_conv/60; // elde edilen float say� 60'a b�l�nerek dakika de�erinin y�zdelik hali elde edildi (59,714/60=0,99523 gibi)
   
   sprintf(lat_dnm,"%.6f",lat_float_conv); // float de�i�ken string'e �evrildi
   
   strncat(loc_inf,latitude,4); // enlem bilgisinin ilk 4 byte � eklendi
   
   memmove(lat_dnm,lat_dnm+2,9); strxfrm(dnm,lat_dnm,2);  // '.' dan sonraki 6 hane 2 li olarak gruplanarak konum bilgisine eklendi
   lat=atoi(dnm);                                         // �r: .325647 -> 3 ve 2 hanesi integer olarak 32 ye d�n��t�r�lerek 32 nin ascii kar��l��� yaz�ld�
   sprintf(dnm,"%c",lat);
   strncat(loc_inf,dnm,1);
   
   memmove(lat_dnm,lat_dnm+2,9); strxfrm(dnm,lat_dnm,2);
   lat=atoi(dnm);
   sprintf(dnm,"%c",lat);
   strncat(loc_inf,dnm,1);
   
   memmove(lat_dnm,lat_dnm+2,9); strxfrm(dnm,lat_dnm,2);
   lat=atoi(dnm);
   sprintf(dnm,"%c",lat);
   strncat(loc_inf,dnm,1);
   
   strncat(loc_inf,space1,1);
   
/////////////////////////////////////  longitude hesaplanan k�s�m

   //gps_array[29]="0",gps_array[30]="3",gps_array[31]="2",gps_array[33]="1",gps_array[34]="5",gps_array[35]="0",gps_array[36]="8",gps_array[37]="4",gps_array[38]="3",gps_array[39]="7";
   //gps_array[41]="W";
   
   if(gps_array[datastart+25]=='W') 
   { 
      sign=45; //-
   }
   else
   {
      sign=43; //+
   }
   
   sprintf(longitude,"%c%c%c%c%c",sign,gps_array[datastart+13],gps_array[datastart+14],gps_array[datastart+15],dot); //boylam bilgisinin desimal k�sm�n� veren 2 digit birle�tirilerek tek bir say� elde edildi ( 5,3 -> 53 gibi)
   
   sprintf(long_float,"%c%c.%c%c%c%c%c",gps_array[datastart+16],gps_array[datastart+17], gps_array[datastart+19],                         //boylam bilgisinin virg�lden sonras�n� veren 7 digit
                                        gps_array[datastart+20],gps_array[datastart+21],gps_array[datastart+22],gps_array[datastart+23]); //birle�tirilerek tek bir say� elde edildi ( 1,2,3,4,5,6,7 -> 12.34567 gibi)
                                                                                                                                             
   long_float_conv=atof(long_float);  // birle�tirilerek olu�turulan virg�ll� say� float format�na d�n��t�r�ld�
   
   long_float_conv=long_float_conv/60; // elde edilen float say� 60'a b�l�nerek dakika de�erinin y�zdelik hali elde edildi (59,714/60=0,99523 gibi)
   
   sprintf(long_dnm,"%.6f",long_float_conv); // float de�i�ken string'e �evirildi
      
   strncat(loc_inf,longitude,5); // boylam bilgisinin ilk 5 byte � eklendi
   
   memmove(long_dnm,long_dnm+2,9); strxfrm(dnm,long_dnm,2); // '.' dan sonraki 6 hane 2 li olarak gruplanarak konum bilgisine eklendi
   log=atoi(dnm);                                           // �r: .325647 -> 3 ve 2 hanesi integer olarak 32 ye d�n��t�r�lerek 32 nin ascii kar��l��� yaz�ld�
   sprintf(dnm,"%c",log);
   strncat(loc_inf,dnm,1);
   
   memmove(long_dnm,long_dnm+2,9); strxfrm(dnm,long_dnm,2);
   log=atoi(dnm);
   sprintf(dnm,"%c",log);
   strncat(loc_inf,dnm,1);
   
   memmove(long_dnm,long_dnm+2,9); strxfrm(dnm,long_dnm,2);
   log=atoi(dnm);
   sprintf(dnm,"%c",log);
   strncat(loc_inf,dnm,1);
   
//!   for(int p=0; p<20; p++)
//!   {
//!      location[p]=loc_inf[p];
//!   }
   
}
