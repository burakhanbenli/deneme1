#define FLASH_SELECT   PIN_C5 
#define FLASH_CLOCK    PIN_B6 
#define FLASH_DI_SCL   PIN_B8 
#define FLASH_DO_SDA   PIN_B9
#define FLASH_RST      PIN_B5

#define FLASH_BUFFER_SIZE    512     // bytes per page 
#define FLASH_BUFFER_COUNT   4096    // page count
#define FLASH_SIZE ((int32)FLASH_BUFFER_SIZE*FLASH_BUFFER_COUNT)  // The size of the flash device in bytes 

// Used in ext_flash_BufferToPage() 
#define ERASE           1  // The flash device will initiate an erase before writing 
#define NO_ERASE        0  // The flash device will not initiate an erase before writing
#define mem_PAGE        0  //For Erase type
#define mem_BLOCK       1  //For Erase type
#define mem_SECTOR      2  //For Erase type
#define mem_CHIP        3  //For Erase type
#define withBuffer      1  //For Read/Write type
#define withoutBuffer   0  //For Read/Write type

#define EXT_FLASH_WRITE_BUFFER   0


void buffer_clear(int8 buf_no);

// Purpose:       Initialize the pins that control the flash device. 
//                This must be called before any other flash function is used. 
// Inputs:        None 
// Outputs:       None 
// Dependencies:  None 
void init_ext_flash() 
{ 
   output_low(FLASH_CLOCK); 
   output_high(FLASH_SELECT);
   output_high(FLASH_RST); 
   
   //write_char12_16_Col(10,2,flash_disp[3],GREEN,1,WHITE,0);  //FLASH INIT OLDU
}
// Purpose:       Send some data to the flash device 
// Inputs:        1) Up to 16 bits of data 
//                2) The number of bits to send 
// Outputs:       None 
// Dependencies:  None 
void ext_flash_sendData(int16 data, int8 size) 
{ 
   int8 i; 
   data <<= (16-size); 
   for(i=0; i<size; ++i) 
   { 
      output_bit(FLASH_DI_SCL, shift_left(&data,2,0));    // Send a data bit 
      output_high(FLASH_CLOCK);                       // Pulse the clock 
      output_low(FLASH_CLOCK); 
   } 
}
// Purpose:       Send some bytes of data to the flash device 
// Inputs:        1) A pointer to an array of data to send 
//                2) The number of bytes to send 
// Outputs:       None 
// Dependencies:  None 
void ext_flash_sendBytes(BYTE* data, int16 size) 
{ 
   int16 i; 
   signed int8  j; 
   for(i=0; i<size; ++i) 
   { 
      for(j=7; j>=0; --j) 
      { 
         output_bit(FLASH_DI_SCL, bit_test(data[i], j));  // Send a data bit 
         output_high(FLASH_CLOCK);                    // Pulse the clock 
         output_low(FLASH_CLOCK); 
      } 
   } 
}
// Purpose:       Wait until the flash device is ready to accept commands 
// Inputs:        None 
// Outputs:       None 
// Dependencies:  ext_flash_sendData() 
void ext_flash_waitUntilReady() 
{ 
   output_low(FLASH_SELECT);                 // Enable select line 
   ext_flash_sendData(0xD7, 8);              // Send status command
   while(!input(FLASH_DO_SDA));                  // Wait until ready 
   output_high(FLASH_SELECT);                // Disable select line 
}
// Purpose:       Get a byte of data from the flash device. This function is 
//                meant to be used after ext_flash_startContinuousRead() has 
//                been called to initiate a continuous read. 
// Inputs:        None 
// Outputs:       1) A byte of data 
// Dependencies:  None 
BYTE ext_flash_getByte() 
{ 
   BYTE flashData; 
   int i; 
   for(i=0; i<8; ++i)                        // Get 8 bits of data 
   { 
      output_high(FLASH_CLOCK); 
      shift_left(&flashData, 1, input(FLASH_DO_SDA)); 
      output_low(FLASH_CLOCK); 
   } 
   return flashData; 
} 
// Purpose:       Get a byte of data from the flash device. This function is 
//                meant to be used after ext_flash_startContinuousRead() has 
//                been called to initiate a continuous read. This function is 
//                also used by ext_flash_readPage() and ext_flash_readBuffer(). 
// Inputs:        1) A pointer to an array to fill 
//                2) The number of bytes of data to read 
// Outputs:       None 
// Dependencies:  None 
void ext_flash_getBytes(BYTE* data, int16 size) 
{ 
   int16 i; 
   signed int8  j; 
   for(i=0; i<size; ++i) 
   { 
      for(j=0; j<8; ++j) 
      { 
         output_high(FLASH_CLOCK); 
         shift_left(data+i, 1, input(FLASH_DO_SDA)); 
         output_low(FLASH_CLOCK); 
      } 
   } 
}
// Purpose:       This function will start reading a continuous stream of
//                data from the entire flash device.
// Inputs:        1) A page address
//                2) An index into the page
// Outputs:       None
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady()
void ext_flash_startContinuousRead(int16 pageAddress, int16 pageIndex)          //12PageAdres+9Index+32Dummy
{
   ext_flash_waitUntilReady();
   output_low(FLASH_SELECT);                 // Enable select line
   ext_flash_sendData(0xE8, 8);              // Send opcode
   ext_flash_sendData(pageAddress, 12);      // Send page address
   ext_flash_sendData(pageIndex, 9);         // Send index and 32 bits
   ext_flash_sendData(0, 32);                // Send 32 don't care bits
}
// Purpose:       Stop continuously reading data from the flash device.
// Inputs:        None
// Outputs:       None
// Dependencies:  None
void ext_flash_stopContinuousRead()
{
   output_high(FLASH_SELECT);                // Disable select line
}
                                                                               ///////////////////////********** READ **********//////////////////////////////
// Purpose:       Read data from a buffer 
// Inputs:        1) A buffer number (0 or 1) 
//                2) An index into the buffer to start reading at 
//                3) A pointer to a data array to be filled 
//                4) The number of bytes of data to read 
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady(), ext_flash_getBytes() 
void ext_flash_readBuffer(int1 bufferNumber, int16 bufferAddress, BYTE* data, int16 size) //15Dummy+9BufferAdres+8Dummy
{ 
   BYTE opcode; 

   output_low(FLASH_SELECT);                 // Enable select line 

   if(bufferNumber) 
      opcode = 0xD6;                         // Opcode for second buffer 
   else 
      opcode = 0xD4;                         // Opcode for first buffer 

   ext_flash_sendData(opcode, 8);            // Send opcode 
   ext_flash_sendData(0, 15);                // Send 15 don't care bits 
   ext_flash_sendData(bufferAddress, 9);     // Send buffer address 
   ext_flash_sendData(0, 8);                 // Send 8 don't care bits 
   ext_flash_getBytes(data, size);           // Get data from the flash device 
   output_high(FLASH_SELECT);                // Disable select line 
}
// Purpose:       Get the data from a page and put it in a buffer
// Inputs:        1) A page address
//                2) A buffer number (0 or 1)
// Outputs:       None
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady()
void ext_flash_PageToBuffer(int16 pageAddress, int1 bufferNumber)                //3Dummy+12PageAdres+9Dummy   200us
{
   BYTE opcode;
   ext_flash_waitUntilReady();
   output_low(FLASH_SELECT);                 // Enable select line

   if(bufferNumber)
      opcode = 0x55;                         // Opcode for second buffer
   else
      opcode = 0x53;                         // Opcode for first buffer

   ext_flash_sendData(opcode, 8);            // Send opcode
   ext_flash_sendData(0, 3);                 // Send 3 don't care bits
   ext_flash_sendData(pageAddress, 12);      // Send page address
   ext_flash_sendData(0, 9);                 // Send 9 don't care bits
   output_high(FLASH_SELECT);                // Disable select line
}
// Purpose:       Read data from a memory page. 
// Inputs:        1) A page address 
//                2) An index into the page to start reading at 
//                3) A pointer to a data array to fill 
//                4) The number of bytes of data to read 
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady(), ext_flash_getBytes() 
void ext_flash_readPage(int16 pageAddress, int16 pageIndex, BYTE* data, int16 size) //3Dummy+12PageAdres+9Index+32Dummy
{ 
   ext_flash_waitUntilReady();               // Wait until ready 
   output_low(FLASH_SELECT);                 // Enable select line 
   ext_flash_sendData(0xD2, 8);              // Send opcode and 5 bits 
   ext_flash_sendData(0, 3);                 // Send 3 don't care bits 
   ext_flash_sendData(pageAddress, 12);      // Send page address
   ext_flash_sendData(pageIndex, 9);         // Send index 
   ext_flash_sendData(0, 16);                // Send 16 don't care bits
   ext_flash_sendData(0, 16);                // Send 16 don't care bits 
   ext_flash_getBytes(data, size);           // Get data from the flash device 
   output_high(FLASH_SELECT);                // Disable select line 
}
                                                                               ///////////////////////********** WRITE **********/////////////////////////////
// Purpose:       Write data to a buffer 
// Inputs:        1) A buffer number (0 or 1) 
//                2) An index into the buffer 
//                3) A pointer to the data to write 
//                4) The number of bytes of data to write 
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady(), ext_flash_sendBytes() 
void ext_flash_writeToBuffer(int1 bufferNumber, int16 bufferAddress, BYTE* data, int16 size) //15dummy+9BufferAdres
{
   BYTE opcode; 

   output_low(FLASH_SELECT);                 // Enable select line 

   if(bufferNumber) 
      opcode = 0x87;                         // Opcode for second buffer 
   else 
      opcode = 0x84;                         // Opcode for first buffer 

   ext_flash_sendData(opcode, 8);            // Send opcode 
   ext_flash_sendData(0, 15);                // Send 15 don't care bits 
   ext_flash_sendData(bufferAddress, 9);     // Send buffer address 
   ext_flash_sendBytes(data, size);          // Write data to the buffer 
   output_high(FLASH_SELECT);                // Disable select line 
}

// Purpose:       Write the data in a buffer to a page 
// Inputs:        1) A page address 
//                2) A buffer number (0 or 1) 
//                3) The writing mode to use 
//                   - Use ERASE to first erase a page then write 
//                   - Use NO_ERASE to write to a previously erased page 
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady() 
void ext_flash_BufferToPage(int1 bufferNumber, int16 pageAddress, int1 mode)    //3Dummy+12PageAdres+9Dummy 15-40ms for Erase, 3-6ms for No erase
{
   BYTE opcode; 
   ext_flash_waitUntilReady();               // Wait until ready 
   output_low(FLASH_SELECT);                 // Enable select line 
   if(mode) //ERASE
   { 
      if(bufferNumber) 
         opcode = 0x86;                      // Opcode for second buffer 
      else 
         opcode = 0x83;                      // Opcode for first buffer 
   } 
   else //NO_ERASE
   { 
      if(bufferNumber) 
         opcode = 0x89;                      // Opcode for second buffer 
      else 
         opcode = 0x88;                      // Opcode for first buffer 
   } 
   ext_flash_sendData(opcode, 8);            // Send opcode
   ext_flash_sendData(0, 3);                 // Send 3 don't care bits 
   ext_flash_sendData(pageAddress, 12);      // Send page address
   ext_flash_sendData(0, 9);                 // Send 9 don't care bits 
   output_high(FLASH_SELECT);                // Disable select line 
}
// Purpose:       Write data to a page through a buffer 
// Inputs:        1) The address of the page to write to 
//                2) The number of the buffer to use (0 or 1) 
//                3) The index into the buffer to start writing at 
//                4) A pointer to the data to write 
//                5) The number of bytes of data to write 
//                6) The writing mode to use 
//                   - Use ERASE to first erase a page then write 
//                   - Use NO_ERASE to write to a previously erased page 
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady(), ext_flash_sendBytes() 
void ext_flash_writePageThroughBuffer(int16 pageAddress, 
                                      int1 bufferNumber, int16 bufferAddress, 
                                      BYTE* data, int16 size, int1 mode)        //3Dummy+12PageAdres+9BufferAdres 15-40ms for Erase, 3-6ms for No erase
{ 
   BYTE opcode; 
   ext_flash_waitUntilReady(); 
   output_low(FLASH_SELECT);                 // Enable select line 
   if(mode) //ERASE
   { 
      if(bufferNumber) 
         opcode = 0x85;                      // Opcode for second buffer 
      else 
         opcode = 0x82;                      // Opcode for first buffer 
   } 
   else //NO_ERASE
   { 
      opcode = 0x02;                         // Opcode for second buffer
   }

   ext_flash_sendData(opcode, 8);            // Send opcode 
   ext_flash_sendData(0, 3);                 // Send 3 don't care bits 
   ext_flash_sendData(pageAddress, 12);      // Send page address
   ext_flash_sendData(bufferAddress, 9);     // Send buffer address 
   ext_flash_sendBytes(data, size);          // Write data to the buffer 
   output_high(FLASH_SELECT);                // Disable select line 
}
                                                                               ///////////////////////********** ERASE **********/////////////////////////////
// Purpose:       Erase a page 
// Inputs:        A page address 
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady() 
void ext_flash_erasePage(int16 pageAddress)                                     //3Dummy+12PageAdres+9Dummy 12-35ms
{ 
   ext_flash_waitUntilReady(); 
   output_low(FLASH_SELECT);                 // Enable select line 
   ext_flash_sendData(0x81, 8);              // Send opcode 
   ext_flash_sendData(pageAddress, 15);      // Send page address with 3 don't care bits
   ext_flash_sendData(0, 9);                 // Send 9 don't care bits 
   output_high(FLASH_SELECT);                // Disable select line 
} 


// Purpose:       Erase a block of 8 pages 
// Inputs:        A block address 
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady() 
void ext_flash_eraseBlock(int8 blockAddress)                                    //3Dummy+9BlockAdres+12Dummy 45-100ms
{ 
   ext_flash_waitUntilReady(); 
   output_low(FLASH_SELECT);                 // Enable select line 
   ext_flash_sendData(0x50, 8);              // Send opcode 
   ext_flash_sendData(blockAddress, 12);     // Send block address with 3 don't care bits
   ext_flash_sendData(0, 12);                // Send 12 don't care bits 
   output_high(FLASH_SELECT);                // Disable select line 
}
// Purpose:       Erase a sector 
// Inputs:        A sector address 
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady() 
void ext_flash_eraseSector(int8 sectorAddress)                                  //3Dummy+4SectorAdres+17Dummy 1,4-3,5s
{ 
   ext_flash_waitUntilReady(); 
   output_low(FLASH_SELECT);                 // Enable select line 
   ext_flash_sendData(0x7C, 8);              // Send opcode 
   ext_flash_sendData(0, 3);                 // Send 3 don't care bits 
   ext_flash_sendData(sectorAddress, 4);     // Send sector address
   ext_flash_sendData(0, 15);                // Send 15 don't care bits
   ext_flash_sendData(0, 2);                 // Send 2 don't care bits 
   output_high(FLASH_SELECT);                // Disable select line 
}
// Purpose:       Erase chip 
// Inputs:        None 
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady() 
void ext_flash_eraseChip()                                                      //4 bytes Opcode 22-40s
{ 
   ext_flash_waitUntilReady(); 
   output_low(FLASH_SELECT);                 // Enable select line 
   ext_flash_sendData(0x7C, 8);              // Send opcode 
   ext_flash_sendData(0x94, 8);              // Send opcode 
   ext_flash_sendData(0x80, 8);              // Send opcode 
   ext_flash_sendData(0x9A, 8);              // Send opcode 
   output_high(FLASH_SELECT);                // Disable select line 
}
                                                                               ///////////////////////********** ORHERS **********////////////////////////////
// Purpose:       Compare the data in a page to the data in a buffer
// Inputs:        1) A page address
//                2) A buffer number (0 or 1)
// Outputs:       1 if the data is the same, 0 if the data is not the same
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady()
int1 ext_flash_comparePageToBuffer(int16 pageAddress, int1 bufferNumber)
{
   int1 CompareFlag;
   BYTE opcode;
   ext_flash_waitUntilReady();
   output_low(FLASH_SELECT);                 // Enable select line

   if(bufferNumber)
      opcode = 0x61;                         // Opcode for second buffer
   else
      opcode = 0x60;                         // Opcode for first buffer

   ext_flash_sendData(opcode, 8);            // Send opcode
   ext_flash_sendData(pageAddress, 15);      // Send page address with 3 don't care bits
   ext_flash_sendData(0, 9);                 // Send 9 don't care bits
   output_high(FLASH_SELECT);                // Disable select line

   output_low(FLASH_SELECT);                 // Enable select line
   ext_flash_sendData(0xD7, 8);              // Send status command
   while(!input(FLASH_DO_SDA));                  // Wait until ready
   output_high(FLASH_CLOCK);                 // Pulse the clock
   output_low(FLASH_CLOCK);
   CompareFlag = !input(FLASH_DO_SDA);           // Get the compare flag
   output_high(FLASH_SELECT);                // Disable select line

   return CompareFlag;
}
// Purpose:       Rewrite the data in a page.
//                The flash device does this by transfering the data to a
//                buffer, then writing the data back to the page.
// Inputs:        1) A page address
//                2) A buffer number (0 or 1)
// Outputs:       None
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady()
void ext_flash_rewritePage(int16 pageAddress, int bufferNumber)
{
   BYTE opcode;
   ext_flash_waitUntilReady();
   output_low(FLASH_SELECT);                 // Enable select line

   if(bufferNumber == 1)
      opcode = 0x58;
   else
      opcode = 0x59;

   ext_flash_sendData(opcode, 8);            // Send opcode
   ext_flash_sendData(pageAddress, 15);      // Send page address with 3 don't care bits
   ext_flash_sendData(0, 9);                 // Send 9 don't care bits
   output_high(FLASH_SELECT);                // Disable select line
}

                                                                               ////////////////////********** My Functions **********/////////////////////////
// Purpose:       Write data to a page with_Buffer or without_Buffer 
// Inputs:        1) The number of the buffer to use (0 or 1) 
//                2) The index into the buffer to start writing at
//                3) The address of the page to write to 
//                4) A pointer to the data to write 
//                5) The number of bytes of data to write
//                6) The writing mode to use 
//                   - Use with_Buffer
//                   - Use without_Buffer
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady(), ext_flash_sendBytes() 
void mem_writeData(int1 bufferNumber, int16 st_Ind, int16 pageAddress, BYTE* data, int16 dataSize, int1 Buffer)
{
   if (Buffer) //Buffer kullanylarak
   {
      ext_flash_PageToBuffer(pageAddress,bufferNumber); //Ystenilen Pagedeki veriler buffer'a �ekildi
      delay_us(250);
      ext_flash_writeToBuffer(bufferNumber,st_Ind,data,dataSize);   //Girilen Datalar buffer'a yazyldy
      ext_flash_BufferToPage(bufferNumber,pageAddress,ERASE);  //Buffer, Page'e yazyldy
      delay_ms(15);
   }
   else  //Buffer kullanylmayarak
   {
      ext_flash_writePageThroughBuffer(pageAddress,bufferNumber,st_Ind,data,dataSize,ERASE);
      delay_ms(15);
   }
   //write_char12_16_Col(10,2,flash_disp[2],GREEN,1,WHITE,0);  //DATALAR YAZILDI
}

void mem_write1Setting(int16 st_Ind, BYTE* data)
{
      ext_flash_PageToBuffer(4095,0); //Ystenilen Pagedeki veriler buffer'a �ekildi
      delay_us(250);
      ext_flash_writeToBuffer(0,st_Ind,data,1);   //Girilen Datalar buffer'a yazyldy
      ext_flash_BufferToPage(0,4095,ERASE);  //Buffer, Page'e yazyldy
      delay_ms(15);
}

void mem_read1Setting(int16 st_Ind, BYTE* data)
{
      ext_flash_PageToBuffer(4095,0);  //Ystenilen Pagedeki veriler buffer'a �ekildi
      delay_us(250);
      ext_flash_readBuffer(0,st_Ind,data,1);   //Bufferdaki istenilen adreslerdeki datalar okundu
}

// Purpose:       Read data to a page with_Buffer or without_Buffer 
// Inputs:        1) The number of the buffer to use (0 or 1) 
//                2) The index into the buffer to start reading at
//                3) The address of the page to read to 
//                4) A pointer to the data to read 
//                5) The number of bytes of data to read
//                6) The reading mode to use 
//                   - Use with_Buffer
//                   - Use without_Buffer
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady(), ext_flash_sendBytes() 
void mem_readData(int1 bufferNumber, int16 st_Ind, int16 pageAddress, BYTE* data, int16 dataSize, int1 Buffer)
{
   if (Buffer) //Buffer kullanylarak
   {
      ext_flash_PageToBuffer(pageAddress,bufferNumber);  //Ystenilen Pagedeki veriler buffer'a �ekildi
      delay_us(250);
      ext_flash_readBuffer(bufferNumber,st_Ind,data,dataSize);   //Bufferdaki istenilen adreslerdeki datalar okundu
   }
   else  //Buffer kullanylmadan
   {
      ext_flash_readPage(pageAddress,st_Ind,data,dataSize);
   }
   //write_char12_16_Col(10,2,flash_disp[1],GREEN,1,WHITE,0);  //DATALAR OKUNDU
}
// Purpose:       Erase data 
// Inputs:        1) Address (Page or Block or Sector) 
//                2) Erase Type (Page or Block or Sector or Chip)
// Outputs:       None 
// Dependencies:  ext_flash_sendData(), ext_flash_waitUntilReady() 
void mem_eraseData(int16 Address, int8 type)
{
   if (type == mem_PAGE)   //Ystenilen Page silinir   DENENDI SILDI
   {
      ext_flash_erasePage(Address);
      delay_ms(15);
   }
   else if (type == mem_BLOCK)   //Ystenilen Block silinir
   {
      ext_flash_eraseBlock(Address);
      delay_ms(50);
   }
   else if (type == mem_SECTOR)  //Ystenilen Sector silinir
   {
      ext_flash_eraseSector(Address);
      delay_ms(1500);
   }
   else if (type == mem_CHIP) //Chip tamamen silinir  //DENENDI SILMEDI
   {
      ext_flash_eraseChip();
      delay_ms(25000);
   }
   
   buffer_clear(2);
   
    //write_char12_16_Col(10,2,flash_disp[0],GREEN,1,WHITE,0);  //DATALAR SILINDI
}


