#include <icob1vp.h>

#INT_RDA///alc - cam -
void  rda_isr(void) 
{
   disable_interrupts(INT_RDA);
   sound_buzzer(400,150,1);
   if(kbhit(P0)){
      data=getc(P0);
      if(data==STX){
         while(!rda_int && countd<5000){
            countd++;
            if(kbhit(P0)){
               data=getc(P0);
               test_[data_counter][1]=data;
               data_counter++;
               countd=0; 
               if(data==uartend){rda_int=1; data_counter=0;}
            }
         }
      }
   }
   
   enable_interrupts(INT_RDA);
   
}

#INT_RDA2///gps - sim - cam -
void  rda2_isr(void) 
{
   disable_interrupts(INT_RDA2);
   //sim uart interrupt
   if(uart2_st==sim){
      bt_data=getch(P1);
      if(!data_geldi){
         sayac=0; data_counter=0;
         sim_data[0][0]=bt_data; sim_data[0][1]=0;
         data_geldi=1;
         do{
            sayac++;
            if(kbhit(P1)){
               receive_data=getch(P1);
               data_counter++;
               sim_data[data_counter][0]=receive_data;
               sim_data[data_counter][1]=NULL;
            }
            if(sayac>65500) break;
         }while(data_counter<100);
      }
   }
   //gps uart interrupt
   if(uart2_st==gps){
      gps_data=getc(P1);
   
      SWITCH (gps_data)
      {
         CASE 'R':
         sayac=0;
         BREAK;
      }
      gps_array[sayac++]=gps_data;
   }
   
   enable_interrupts(INT_RDA2);
}

#INT_EXT0///pwr_cnt - 
void  ext0_isr(void) 
{  
   disable_interrupts(INT_EXT0);
   INT0EP = 1; //INT0 H to L
   vin=0;
   enable_interrupts(INT_EXT0);
}

#INT_EXT1///acc_cnt - 
void  ext1_isr(void) 
{  
   disable_interrupts(INT_EXT1);
   sound_buzzer(400,150,1);
   delay_ms(500);
   if((int1st==acc) && (acc_mod_on==0)){
      acc_mod_on=1;
      INT1EP = 0; //INT1 L to H
      ext_int_edge(1, L_TO_H);
   }
   else if((int1st==acc) && (acc_mod_on==1)){
      acc_mod_on=0;
      INT1EP = 1; //INT1 H to L
      ext_int_edge(1, H_TO_L);
   }
   enable_interrupts(INT_EXT1);
}

#INT_EXT2///alarm - chg_st - 
void  ext2_isr(void) 
{
   disable_interrupts(INT_EXT2);
   sound_buzzer(400,150,1);
   delay_ms(1000);
   
   if(lock==1){lock=0; break;}
   
   if(alarmst==0 && retest==1 && acc_mod_on==1) minute++;
   
   if((alarmst==1) && (acc_mod_on==1)){
      retest=1;
      wait_retest=0;
      test_st=1;
   }
   
   if((retest) && (acc_mod_on==0)){
      minute++;
   }
   
   if(alarmst==2){
      minute++;
      if(minute==2){
         minute=0;
         alarmst=3;
      }
   }
   
   enable_interrupts(INT_EXT2);
}

//!#INT_TIMER1///
//!void  timer1_isr(void) 
//!{
//!
//!}

void main()
{

   set_tris_a(0x050f);
   set_tris_b(0x409c);
   set_tris_c(0x0004);

   output_c(0x0000);
   output_b(0x0400);
   output_a(0x0000);
   
   output_float(PIN_B2);
   output_float(PIN_B3);
   output_float(PIN_B4);

   output_low(charge);  
   
   U1EN = 1;  
   U1WAKE = 1;
   //U1RXIE=1; 
   
   U2EN = 1;  
   U2WAKE = 1;
   
   INT0EP = 0;
   INT1EP = 1;
   INT2EP = 0;
   INT1EN = 1;
   INT2EN = 1;

//!   ext_int_edge(L_TO_H); //INT0 L to H   
   
   //setup_timer1(TMR_INTERNAL | TMR_DIV_BY_256, 156);
   setup_adc_ports(sAN0 | sAN1, VSS_VDD);
   setup_adc(ADC_CLOCK_INTERNAL);
   
   disable_interrupts(INT_EXT0);
   disable_interrupts(INT_EXT1);
   disable_interrupts(INT_EXT2);
   disable_interrupts(INT_RDA2);
   disable_interrupts(INT_RDA);
   disable_interrupts(INT_ADC1);
   disable_interrupts(GLOBAL);

//!   enable_interrupts(INT_EXT0);
   clear_interrupt(INT_RDA);

//!   setint2(clk); //interrupt2 clk i�in setleniyor
   
//!   sound_buzzer(400,150,1);//YAZILIM BA�LANGI� BUZZERI
   output_high(BUZZER);
   delay_ms(500);
   output_low(BUZZER);
   delay_ms(500);

   vin_lvl_det();
   //wiring_check();

   output_high(PWR_EN);
   output_low(pwr_5v);
   
   //get_settings();
   
   sound_buzzer(400,150,1); //AYARLAR YAPILDI BUZZERI
   
   DELAY_MS(200);
//!   clear_interrupt(INT_RDA);
   
   pin_select("INT1",acc_cnt,TRUE,FALSE);
   pin_select("INT2",Rtc_CLK,TRUE,FALSE);
   enable_interrupts(GLOBAL);
   ENABLE_INTERRUPTS(INT_EXT1);
   ENABLE_INTERRUPTS(INT_EXT2);
   ext_int_edge( 2, L_TO_H);
   ext_int_edge( 1, H_TO_L);
   
   if(!input(acc_cnt)) acc_mod_on=1;
   else acc_mod_on=0;
   
   while(TRUE){

      while(acc_mod_on==0){
      
         output_low(ign_12V);
         output_low(ign_24V);
         output_low(horn_lamp_12V);
         output_low(horn_lamp_24V);
         
         if(wait_retest==1){
            /*tekrar testi istenmeden ara� kapat�lm��sa;(wait_restest=1)
            5 dk l�k bir geri say�m koy e�er bu s�re i�inde tekrar �al��t�r�lmazsa sleepe git
            e�er tekrar �al��t�r�l�rsa alarm� beklemeye devam et*//////OK!
            for(int i=0;i<301;i++){
               delay_ms(995);
//!               wiring_check();
//!               ign_check();
               if(acc_mod_on==1){ 
                  if(vin_lvl==0) output_high(ign_12V);
                  else if(vin_lvl==1) output_high(ign_24V);
                  //do{
                     if(!input(Inf1)){
                        delay_ms(10000);
                        stall_time_delay=1;
                        output_low(ign_12V);
                        output_low(ign_24V);
                        goto LABEL2;
                        //break;
                     } 
                  //}while(1);
                  //goto LABEL2;
               }
               else if(!acc_mod_on){
                  output_low(ign_12V);
                  output_low(ign_24V);
               }
            }
         output_low(ign_12V);
         output_low(ign_24V);
         wait_retest=0;
         disable_interrupts(INT_EXT2);
         if(acc_mod_on==1)goto LABEL2;
         }
         else if(retest==1){
            /*tekrar testi istenmi� ve ara� kapat�lm��sa;
            bir s�re sesli ve g�r�nt�l� ikaz vererek test yap�lmas�n� iste
            testin yap�l�p yap�lmad���n� yap�ld�ysa sonu�lar� kay�t alt�na al;*////OK!!????? OK DE��L BUNU NE ���N KOYMU�UM B�LM�YORUM!!
            goto LABEL2;
         }
         
         //wiring_check();
         // if(wiring_ok==0)  kablo ba�lant�s�nda m�dahale var!! 
         // else              yap�lmak istenen ne?
         
         //ign_check();
         //if(ign_on==1 & ign_perm==0)    motora izinsiz ign verildi!!
         //else                           yap�lmak istenen ne?

         setsleep();
         fprintf(P0,"%c%c%c",STX,waked_up,uartend);
         DELAY_MS(1000);
//!         sound_buzzer(800,150,1);
         
      }
      
      
      while(acc_mod_on){
         LABEL2:
         //battery_check();
         ignition_test();
      }
   }

}

//!////////// batarya seviyesi kontrol edilerek �arj durumuna karar veriliyor...
void battery_check()
{
   set_adc_channel(0);
   delay_us(100);

   for(int i=0;i<20;i++){
      bat_val = read_adc();
      delay_us(100);
      bat_val_total = bat_val_total + bat_val;
      if(bat_val<=590)bat_val_cntr++;
   }
   bat_val=bat_val_total/20;
   
   if(bat_val_cntr<12){
      bat_low=0;
      output_low(charge);
      sound_buzzer(600,150,1);
      delay_ms(5000);
      if(!acc_mod_on){
         pin_select("INT2",Rtc_CLK,TRUE,FALSE);
         int2st=clk;
         INT2EP = 0; //INT2 L to H  
      }
   }
   else{
      bat_low=1;
      output_high(charge);
      sound_buzzer(200,150,1);
      delay_ms(500);
      if(!acc_mod_on){
         pin_select("INT2",chng_st,TRUE,FALSE);
         int2st=chg;
         INT2EP = 0; //INT2 L to H  +
      }
   }
//!   setup_adc(ADC_OFF);
   bat_val_cntr=0;
   bat_val_total=0;
}

////////// giri�deki gerilim seviyesi kontrol edilerek gerekli atamalar yap�l�yor...
int vin_lvl_det()
{
   set_adc_channel(1);
   delay_us(100);
   vin_sys = read_adc();
   delay_us(100);

   if(vin_sys>465){//1,5V dan b�y�kse 24V giri�
      vin_lvl=1; 
   }
   else if(155<vin_sys<=465){//1,5V dan k���kse 12V giri�
      vin_lvl=0; 
   }
   else low_pwr=1;     
//!   setup_adc(ADC_OFF);
   return vin;
}

//!///////// kablo ba�lant�lar� kotrol ediliyor...
//!int wiring_check(){
//!
//!   output_high(wire_check);
//!   delay_ms(2000);
//!   if(input(wire_st)==0)wiring_ok=1;
//!   else wiring_ok=0;
//!   output_low(wire_check);
//!   return wiring_ok;
//!}

///////// araca ignition verilip verilmedi�ini kontrol ediyor...
//!int ign_check(){
//!
//!   if(input(Inf1)==0)ign_on=1;
//!   else ign_on=0;
//!   return ign_on;
//!}

//////// sound_buzzer (s�re, 1 pulse s�resi)
void sound_buzzer(int16 ms, int16 sq_us, keytonee)
{
   if(keytonee){
      //!//sq_us>500 olmal�
      if(sq_us==100) { sq_us=120; }
      if(ms==10) { ms=15; }
   
      int16 tmp_uto_ms=(sq_us*2)/100;
      int16 tmp=ms/(tmp_uto_ms)*10;
   
      for (tmp_16=0;tmp_16<tmp;tmp_16++)
      {
      //if (ses_ikz) 
      output_high(BUZZER);
      delay_us(sq_us);
      output_low(BUZZER);
      delay_us(sq_us);
      }
   }

}

////////sleep
void setsleep()
{
//!   output_high(PWR_EN);
//!   output_low(pwr_5v);
//!   
//!   setup_uart(0, P0);
//!   setup_uart(0, P1);
//!   
//!   set_tris_a(0x050f);
//!   set_tris_b(0x0084);
//!   set_tris_c(0x0000);
//!   
//!   output_c(0x0000);
//!   output_b(0x0000);
//!   output_a(0x0000);

   fprintf(P0,"%c%c%c",STX,going_to_sleep,uartend);
   test_st=0;
   
   pin_select("INT1",acc_cnt,TRUE,FALSE);
   int1st=acc;
   if(input(acc_cnt)){acc_mod_on=0; ext_int_edge(1, H_TO_L);}
   else {acc_mod_on=1; ext_int_edge(1, L_TO_H);}
//!   pin_select("INT2",Rtc_CLK,TRUE,FALSE);
//!   int1st=clk;
//!   ext_int_edge(2, L_TO_H);     //INT1 H to L
  
   if(lock==1)
   {
      setAlarm(PER_HOUR);///////
      enable_interrupts(INT_EXT0);
      disable_interrupts(INT_EXT1);
      enable_interrupts(INT_EXT2);
   }
   else 
   {
//!      battery_check();
      setAlarm(PER_HOUR);///////
      enable_interrupts(INT_EXT0);
      enable_interrupts(INT_EXT1);      
      disable_interrupts(INT_EXT2);
      disable_interrupts(INT_RDA);
      disable_interrupts(INT_RDA2);
   }
   sound_buzzer(400,150,1);

   PMD2 = 0xFFFF;
   PMD3 = 0xFFFF;
   
   U2EN = 0;
   U1EN = 0;
   set_uart_speed(0,P0);
   set_uart_speed(0,P1);
   
   output_c(0x0000);
   output_b(0x0400);
   output_a(0x0000);
   
//!   output_low(horn_lamp_12V);
//!   output_low(horn_lamp_24V);
//!   output_high(PWR_EN);
//!   output_low(pwr_5v);
   
   sleep(SLEEP_FULL);

   
//!   U1EN=1;  
//!   U1WAKE=1;
//!   //U1RXIE=1; 
//!   INT0EP = 0;
//!   INT1EP = 1;
//!   INT2EP = 0;
//!   INT1EN=1;
//!   INT2EN=1;

   //reset_cpu();
   
//!   PMD1 = 0x0000;
//!   PMD2 = 0x0000;
//!   PMD3 = 0x0000;
   
}

/////// uart set pps///uart ayarlan�yor (setuart1(alc) --> uart alc ile haberle�iyor)...
/////// uint --> alc(alkolmetre), cam(kamera), gps, sim
void setuart1(int unit)
{
   clear_interrupt(INT_RDA);

   if(unit==alc){
      pin_select("U1TX",Alc_Tx,TRUE,FALSE);
      pin_select("U1RX",Alc_Rx,TRUE,FALSE);
      uart1_st=alc;
   }
   else if(unit==cam){
      pin_select("U1TX",Cam_Tx,TRUE,FALSE);
      pin_select("U1RX",Cam_Rx,TRUE,FALSE);
      uart1_st=cam;
   }
   else if(unit==gps){
      pin_select("U1TX",GPS_Tx,TRUE,FALSE);
      pin_select("U1RX",GPS_Rx,TRUE,FALSE);
      uart1_st=gps;
   }
   delay_us(500);
}

void setuart2(int unit)
{
   if(unit==alc){
      pin_select("U2TX",Alc_Tx,TRUE,FALSE);
      pin_select("U2RX",Alc_Rx,TRUE,FALSE);
      uart2_st=alc;
   }
   else if(unit==cam){
      pin_select("U2TX",Cam_Tx,TRUE,FALSE);
      pin_select("U2RX",Cam_Rx,TRUE,FALSE);
      uart2_st=cam;
   }
   else if(unit==gps){
      pin_select("U2TX",GPS_Tx,TRUE,FALSE);
      pin_select("U2RX",GPS_Rx,TRUE,FALSE);
      uart2_st=gps;
   }
   else if(unit==sim){
      pin_select("U2TX",SIM_Tx,TRUE,FALSE);
      pin_select("U2RX",SIM_Rx,TRUE,FALSE);
      uart2_st=sim;
   }
   delay_us(500);
}

////// interrupt set pps//interruptlar ayarlan�yor...
////// uint --> acc(accessory mode), clk(rtc alarm), chg(batarya charge durumu)
void setint1(int uint)
{
   if(uint==clk){
      pin_select("INT1",Rtc_CLK,TRUE,FALSE);
      int1st=clk;
      INT2EP = 0; //INT2 L to H 
   }
   else if(uint==chg){
      pin_select("INT1",chng_st,TRUE,FALSE);
      int1st=chg;
      INT2EP = 0; //INT2 L to H  +
   }
   else if(uint==acc){
      pin_select("INT1",acc_cnt,TRUE,FALSE);
      int1st=acc;
      INT1EP = 1; //INT1 H to L
   }
}

void setint2(int uint)
{
   if(uint==clk){
      pin_select("INT2",Rtc_CLK,TRUE,FALSE);
      int2st=clk;
      INT2EP = 0; //INT2 L to H 
   }
   else if(uint==chg){
      pin_select("INT2",chng_st,TRUE,FALSE);
      int2st=chg;
      INT2EP = 0; //INT2 L to H  +
   }
   else if(uint==acc){
      pin_select("INT2",acc_cnt,TRUE,FALSE);
      int2st=acc;
      INT1EP = 1; //INT1 H to L
   }
}

////// retest, far-korna, gps, sim, kamera ayarlar� al�n�yor...
void get_settings()
{

   output_high(PWR_5v);
   output_low(PWR_en);
   
   setuart1(alc);
   fprintf(P0,"%c%c%c",STX,set_wait,uartend);
   delay_ms(100);
   rda_int=0;
   disable_interrupts(INT_RDA);
   
   while(!rda_int){
      if(kbhit(p0)){
         data=getc(P0);
         if(data==STX){
            while(!rda_int && ll<5000){
            ll++;
               if(kbhit(P0)){
                  data=getc(P0);
                  if(data==uartend) rda_int=1;
                  st[i]=data;
                  ll=0;
                  i++;
               }
            }
         }
      }
   }
   data=0;
   data_counter=0;

   for(int i=2;i<7;i++){////zaman� al
   timereg[i][1]=st[i-1];
   }
   
//!   updateTime();
   
   if(bit_test(st[0],0)){
      retest_on=1;
   }
   else retest_on=0;
   
   if(bit_test(st[0],1)){
      lamphorn_on=1;
   }
   else lamphorn_on=0;
   
   if(bit_test(st[0],2)){
      cam_on=1;
   }
   else cam_on=0;
   
   if(bit_test(st[0],3)){
      gps_on=1;
   }
   else gps_on=0;
   
   if(bit_test(st[0],4)){
      sim_on=1;
   }
   else sim_on=0;
   
   rda_int=0;

}

///// Flash Haf�zaya Data Yazma Fonksiyonu
void data_log(int8 event)
{  
   unsigned int8  flash_buffer[22];
   unsigned int8  mem_set[11];
   
   unsigned int16 last_page = 0;
   unsigned int16 last_byte = 0;
   
   unsigned int8 buffer_size = 0;
   unsigned int8 overflow_cnt = 0;
   unsigned int8 mem_write_end = 0xFF;
   
   mem_read1Setting(11, mem_set);
   
   last_page = make16( mem_set[0], mem_set[1] );
   last_byte = make16( mem_set[2], mem_set[3] );
   
   
   readTime();
   
   if(event==test_start || event==retest_start){
      flash_buffer[5] = event;
      flash_buffer[6] = sonuch;
      flash_buffer[7] = sonucl;
   
      flash_buffer[0] = timereg[3][1];//hour
      flash_buffer[1] = timereg[2][1];//min
      flash_buffer[2] = timereg[4][1];//day
      flash_buffer[3] = timereg[5][1];//mounth
      flash_buffer[4] = timereg[6][1];//year
      
      buffer_size=8; 
   }
   else{///�rnek kabul edilmedi/enine on/engine off/warning given(?)/starter not active/calibration check
      flash_buffer[5] = event;
      
      flash_buffer[0] = timereg[3][1];//hour
      flash_buffer[1] = timereg[2][1];//min
      flash_buffer[2] = timereg[4][1];//day
      flash_buffer[3] = timereg[5][1];//mounth
      flash_buffer[4] = timereg[6][1];//year
      
      buffer_size=6;
      //burada yaz
   }
   
   overflow_cnt=(512 - last_byte);
   
   if(buffer_size>overflow_cnt){
      mem_writeData(0, last_byte, last_page, flash_buffer, buffer_size, 1);
      last_byte = last_byte + buffer_size;
   }
   else if(buffer_size==overflow_cnt){
      mem_writeData(0, last_byte, last_page, flash_buffer, buffer_size, 1);
      last_byte = 0;
      last_page++;
   }
   else{
      mem_writeData(0, last_byte, last_page, flash_buffer, overflow_cnt, 1);
      last_byte = 0;
      last_page++;
      for(int i=0;i<(buffer_size - overflow_cnt);i++){
         flash_buffer[i] = flash_buffer[overflow_cnt+i];
      }
      mem_writeData(0, last_byte, last_page, flash_buffer, (buffer_size - overflow_cnt), 1);
      last_byte = last_byte + (buffer_size - overflow_cnt);
   }  
   
   
   if(gps_on){
      buffer_size=13;
      overflow_cnt=(512 - last_byte);
      
      if(buffer_size>overflow_cnt){
         mem_writeData(0, last_byte, last_page, gsp_loc_buffer, buffer_size, 1);
         last_byte = last_byte + buffer_size;
      }
      else if(buffer_size==overflow_cnt){
         mem_writeData(0, last_byte, last_page, gsp_loc_buffer, buffer_size, 1);
         last_byte = 0;
         last_page++;
      }
      else{
         mem_writeData(0, last_byte, last_page, gsp_loc_buffer, overflow_cnt, 1);
         last_byte = 0;
         last_page++;
         for(int i=0;i<(buffer_size - overflow_cnt);i++){
            gsp_loc_buffer[i] = gsp_loc_buffer[overflow_cnt+i];
         }
         mem_writeData(0, last_byte, last_page, gsp_loc_buffer, (buffer_size - overflow_cnt), 1);
         last_byte = last_byte + (buffer_size - overflow_cnt);
      }
   }
   
   mem_writeData(0, last_byte, last_page, &mem_write_end, 1, 1); //biti�
   if(last_byte==511){
      last_byte=0;
      last_page++;
   }
   else last_byte++;
   
   mem_set[0] = make8(last_page,1);
   mem_set[1] = make8(last_page,0);  
   mem_set[2] = make8(last_byte,1);
   mem_set[3] = make8(last_byte,0); 
   
   mem_write1Setting(4, mem_set);
 
}


