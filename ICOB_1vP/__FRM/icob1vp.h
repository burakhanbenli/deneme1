#include <24HJ64GP504.h>

#device ADC=10

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES NOWRTB                   //Boot block not write protected
#FUSES NOBSS                    //No boot segment
#FUSES NORBS                    //No Boot RAM defined
#FUSES NOWRTSS                  //Secure segment not write protected
#FUSES NOSSS                    //No secure segment
#FUSES NORSS                    //No secure segment RAM
#FUSES NOWRT                    //Program memory not write protected
#FUSES NOIESO                   //Internal External Switch Over mode disabled
#FUSES NOOSCIO                  //OSC2 is clock output
#FUSES NOIOL1WAY                //Allows multiple reconfigurations of peripheral pins
#FUSES NOCKSFSM                 //Clock Switching is disabled, fail Safe clock monitor is disabled
//#FUSES WINDIS                   //Watch Dog Timer in non-Window mode
#FUSES NOPUT                    //No Power Up Timer
#FUSES NOALTI2C1                //I2C1 mapped to SDA1/SCL1 pins
#FUSES NOJTAG                   //JTAG disabled
#FUSES HS
#FUSES NODEBUG
#FUSES PROTECT

#device ICSP=1
#use delay(crystal=24000000)
   

//!#use STANDARD_IO( B )
//!#use FIXED_IO( A_outputs=PIN_A9,PIN_A7,PIN_A4 )
//!#use FIXED_IO( B_outputs=PIN_B15,PIN_B13,PIN_B12,PIN_B11,PIN_B10,PIN_B6,PIN_B5,PIN_B0 )
//!#use FIXED_IO( C_outputs=PIN_C9,PIN_C8,PIN_C7,PIN_C6,PIN_C5,PIN_C4,PIN_C3,PIN_C1,PIN_C0 )

#use fast_io(A)
#use fast_io(B)
#use fast_io(C)

#define Vbat            PIN_A0
#define Vsys            PIN_A1
#define Cam1            PIN_A4
#define pwr_5v          PIN_A7
#define Inf1            PIN_A8
#define Alc1            PIN_A9
#define wire_st         PIN_A10
#define Gps_Tx          PIN_B0
#define Gps_Rx          PIN_B1
#define acc_cnt         PIN_B2
#define Alc_Rx          PIN_B3
#define Rtc_CLK         PIN_B4
//!#define RST_F           PIN_B5
//!#define SCK             PIN_B6
#define pwr_cnt         PIN_B7
//!#define SCL_SI          PIN_B8
//!#define SDA_SO          PIN_B9
#define PWR_EN          PIN_B10
//!#define vign            PIN_B11
#define Ign_12V         PIN_B11
#define Ign_24V         PIN_B12
#define wire_check      PIN_B13
#define SIM_Rx          PIN_B14
#define SIM_Tx          PIN_B15
#define Alc_Tx          PIN_C0
#define Cam_Tx          PIN_C1
#define Cam_Rx          PIN_C2
#define Alc2            PIN_C3
#define buzzer          PIN_C4
//!#define CS              PIN_C5
#define charge          PIN_C6
#define chng_st         PIN_C7
//!#define horn_lamp       PIN_C8
#define horn_lamp_12v   PIN_C8
#define horn_lamp_24v   PIN_C9

#pin_select U1TX=PIN_C0
#pin_select U1RX=PIN_B3
#use rs232(UART1, baud=9600, parity=N, errors, stream=P0)//Alc

#pin_select U2TX=PIN_C1
#pin_select U2RX=PIN_C2
#use rs232(UART2, baud=9600, parity=N, errors, stream=P1)//Cam

//#use rs232(xmit=PIN_B15, rcv=PIN_B14, baud=9600, errors, stream=Sim)
//#use rs232(xmit=PIN_B0, rcv=PIN_B1, baud=9600, errors, stream=Gps)

//!#pin_select SCK1OUT = PIN_B6
//!#pin_select SDI1 = PIN_B8
//!#pin_select SDO1 = PIN_B9
//!#use spi(MASTER, SPI1, BAUD=115200, MODE=3, BITS=8, stream=F0)

//!#use i2c(MASTER, I2C1, SLOW, stream=RV)

#define alc 12
#define cam 13
#define gps 14
#define sim 15
#define acc 16
#define clk 17
#define chg 18

unsigned int16 bat_val=0, bat_val_total=0, vin_sys=0, sayac=0, ctr_URL=0;
int16 tmp_16;
unsigned int8 bat_val_cntr=0, data_counter=0;
unsigned int8 data=0, countd=0, minute=0;
int1 bat_low=0, vin=1, low_pwr=0, vin_lvl=0, acc_mod_on=0, wiring_ok=0, ign_on=0, ign_perm=0;
int1 stall_time_delay=0, wait_retest=0, retest=0, rda_int=0, ses_ikz=1, location_ok=0;
int1 lock=0, quit_perm=0; 
int  alarmst=0, int1st=acc, uart1_st=alc, uart2_st=cam, int2st=acc;
char gps_array[500],gps_data, minuss[15]="";
int1 retest_on=0, lamphorn_on=0, cam_on=0, gps_on=0, sim_on=0, test_st=0,intest_fail_cnt=0; 

unsigned int8 st[];
int16 ll=0;
int i=0;

//sim
unsigned char sim_data[100][2];
char bt_data,receive_data;
int1 data_geldi=0;
unsigned int16 response_ctr=0;
int1 return_ok=0,return_action_ok=0,return_sapbr_2_1_ok=0,return_URL_ok=0;
unsigned int8 send_ctr=0;
unsigned int8 cmd_stops,stop_ctr=0,action_200_ctr=0,httppara_ctr=0;

unsigned int8 cam_buffer[512];
int8 sonuch=0, sonucl=0;


#define STX       170
#define settings  169
#define uartend   241

//alc-durumlar
#define test_start                199
#define retest_start              188
#define retest_warning            177//retest s�resi geldi, ara� kapat�ld�
#define car_off_wait              166//ara� istop edildi yeniden �al��t�r�lmas� i�in 5dk bekleme
#define ign_missed                155//ara� �al��t�r�lmas� i�in tan�nan s�re a��ld�, test tekrarlancak
#define initial_test_success        0//
#define initial_test_unsuccess      1//
#define retest_success            122//
#define retest_unsuccess            2//
#define retest_missed               3//retest s�resi geldi ancak ara� �al��m�yorsa testin yap�lmamas� durumu
#define retest_not_delivered        4//retest s�resi geldi ve ara� �al���yorsa testin yap�lmamas� durumu

#define test_row           0x00
#define test_result_1      0x01
#define test_result_2      0x02
#define test_date_day      0x03    
#define test_date_mounth   0x04    
#define test_date_year     0x05    
#define test_time_minute   0x06
#define test_time_hour     0x07
#define test_loc_lat_1     0x08
#define test_loc_lat_2     0x09
#define test_loc_lat_3     0x0a
#define test_loc_lat_4     0x0b
#define test_loc_lat_5     0x0c
#define test_loc_long_1    0x0d
#define test_loc_long_2    0x0e
#define test_loc_long_3    0x0f
#define test_loc_long_4    0x10
#define test_loc_long_5    0x11
#define test_loc_long_6    0x12
#define test_id            0x13

unsigned int8 test_[19][2]={

   /*{test_row, 0x00},*/ {test_result_1, 0x00}, {test_result_2, 0x00}, 
   {test_date_day, 0x00}, {test_date_mounth, 0x00}, {test_date_year, 0x00},
   {test_time_minute, 0x00}, {test_time_hour, 0x00}, 
   {test_loc_lat_1, 0x00}, {test_loc_lat_2, 0x00}, {test_loc_lat_3, 0x00}, {test_loc_lat_4, 0x00}, {test_loc_lat_5, 0x00}, 
   {test_loc_long_1, 0x00}, {test_loc_long_2, 0x00}, {test_loc_long_3, 0x00}, {test_loc_long_4, 0x00}, {test_loc_long_5, 0x00}, {test_loc_long_6, 0x00}

};

#include <uart_codes.h>

static int setAlarm(unsigned int8 alarm);
static int readTime(void);
static int updateTime(void);

// sim800l
int1 gsm_gprs_sim800L_test();
void sim800l_send_sms();
void read_from_server_GPRS();
void send_command(char *cmd);
int1 check_for_OK_response();
int1 HTTP_ACTION_200_CHECK();
int1 at_sapbr_2_1_check();
int1 URL_cmd_check();
int1 HTTP_READ_CHECK();

void data_log(int8 event);
void setuart1(int unit);
void setuart2(int unit);
void setint1(int unit);
void setint2(int unit);
void battery_check();
int vin_lvl_det();
int wiring_check();
int ign_check();
void sound_buzzer(int16 ms, int16 sq_us,keytonee);
void setsleep();
void get_settings();
void test_res();
void ignition_test();


#include <math.h>
#include <stdlib.h>
#include <string.h>

//
#byte RPINR0 = 0x0680
#bit  INT1R0 = RPINR0.8
#bit  INT1R1 = RPINR0.9

//ext. interrupts edge selection
#byte INTCON2 = 0x0082 
#bit INT2EP = INTCON2.2 //INT2 EDGE SELECT 1=N / 0=P
#bit INT1EP = INTCON2.1 //INT2 EDGE SELECT 1=N / 0=P
#bit INT0EP = INTCON2.0 //INT2 EDGE SELECT 1=N / 0=P

//
#byte RCON  = 0x0740
#bit  BOR   = RCON.1 
#bit  MCLR  = RCON.7 
#bit  SLP   = RCON.3 

//uart settings 
#byte U1MODE  = 0x0220
#bit  U1EN    = U1MODE.15
#bit  U1WAKE  = U1MODE.7
#bit  U1INV   = U1MODE.4

#byte U2MODE  = 0x0230
#bit  U2EN    = U1MODE.15
#bit  U2WAKE  = U1MODE.7
#bit  U2INV   = U1MODE.4

//UART INTERRUPT EN
#byte IEC0    = 0x0094
#bit  U1RXIE  = IEC0.11

#byte IEC1    = 0x0096
#bit  U2RXIE  = IEC1.14

//external int en
#bit  INT0EN  = IEC0.0
#byte IEC1    = 0x0096
#bit  INT1EN  = IEC0.13
#bit  INT2EN  = IEC0.4

//PMD SETTINGS (FOR SLEEP)
#byte PMD1 = 0x0770
#byte PMD2 = 0x0772
#byte PMD3 = 0x0774

//#include "def.h"
#include "AT45DQ161.c"
#include "sft_i2c.c"
#include "gps.c"
#include "rv_1805.c"
#include "sim800l.c"
#include "res_operations.c"
#include "ign_tests.c"


