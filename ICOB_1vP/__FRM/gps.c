unsigned int8 gsp_loc_buffer[14];

int1 GPS_satellite_info(){
   unsigned char glnbyte00[3];
   char nmbr_of_sat_in_use[8];
   
   if(gps_array[144]==49 || gps_array[144]==50){
      itoa(gps_array[144],10,glnbyte00);  
      //write_char12_16_Col( 10, 180,glnbyte00,RED,1,AMBER,0);
      sprintf(nmbr_of_sat_in_use,"NS:%c%c",gps_array[146],gps_array[147]);    // Ka� tane uydunun kullan�ld���n� i�eren datalar (04,08,... vs)
      //write_char12_16_Col( 10, 205,nmbr_of_sat_in_use,RED,1,AMBER,0);//nmbr_of_sat_in_use = ka� uydu kullan�ld��� bilgisi //buras� kendi kodum i�in d�zenlenecek
      
      return(1);
    }
    else return(0);
}

void GPS_latitude_calc(){
   char lat_decimal[8],lat_float[30],dnm[10],x[10];
   float lat_float_conv;
   unsigned int8 lat;

   //gps_array[16]="3",gps_array[17]="9",gps_array[18]="5",gps_array[19]="6",gps_array[21]="1",gps_array[22]="2",gps_array[23]="8",gps_array[24]="4",gps_array[25]="3";
   //gps_array[27]="S";
   
   sprintf(lat_decimal,"%c%c",gps_array[16],gps_array[17]); //enlem bilgisinin desimal k�sm�n� veren 2 digit birle�tirilerek tek bir say� elde edildi ( 5,3 -> 53 gibi)
   lat=atoi(lat_decimal);
   //bu//test_[8][1]=lat;//virg�lden �nceki konum bilgisi kaydedildi
   gsp_loc_buffer[0]=lat;
   
   sprintf(lat_float,"%c%c.%c%c%c%c%c",gps_array[18],gps_array[19],gps_array[21],gps_array[22],gps_array[23],gps_array[24],gps_array[25]); //enlem bilgisinin virg�lden sonras�n� veren 7 digit 
            //birle�tirilerek tek bir say� elde edildi ( 1,2,3,4,5,6,7 -> 12.34567 gibi)
   
   lat_float_conv=atof(lat_float);  // birle�tirilerek olu�turulan virg�ll� say� float format�na d�n��t�r�ld�
   lat_float_conv=lat_float_conv/60; // elde edilen float say� 60'a b�l�nerek dakika de�erinin y�zdelik hali elde edildi (59,714/60=0,99523 gibi)
   sprintf(dnm,"%.7f",lat_float_conv); // float de�i�ken string'e �evirilerek ekrana bas�lmaya uygun hale getirildi
   
   memmove(dnm,dnm+2,9); strxfrm(x,dnm,2); lat=atoi(x);  // k�s�rat k�sm�n�n ba��ndaki 0(s�f�r) ve ,(virg�l)silindi.(0,9646 -> 9646 gibi)    
   //bu//test_[9][1]=lat;  //soldan ba�layarak ikili ikili guruplan�yor ve integer a �evirip diziye yaz�l�yor
   gsp_loc_buffer[1]=lat;
   
   memmove(dnm,dnm+2,9); strxfrm(x,dnm,2); lat=atoi(x); 
   //bu//test_[10][1]=lat;
   gsp_loc_buffer[2]=lat;
   
   memmove(dnm,dnm+2,9); strxfrm(x,dnm,2); lat=atoi(x); 
   //bu//test_[11][1]=lat;
   gsp_loc_buffer[3]=lat;
   
   memmove(dnm,dnm+2,9); strxfrm(x,dnm,2); lat=atoi(x);
   //bu//test_[12][1]=lat;
   gsp_loc_buffer[4]=lat;
}

void GPS_longitude_calc(){
   char long_decimal[8],long_float[30],long_dnm[10],x[10];
   float long_float_conv;
   int lon;
  // gps_array[29]="0",gps_array[30]="3",gps_array[31]="2",gps_array[33]="1",gps_array[34]="5",gps_array[35]="0",gps_array[36]="8",gps_array[37]="4",gps_array[38]="3",gps_array[39]="7";
   //gps_array[41]="W";
   
   sprintf(long_decimal,"%c%c%c",gps_array[29],gps_array[30],gps_array[31]); //boylam bilgisinin desimal k�sm�n� veren 3 digit birle�tirilerek tek bir say� elde edildi ( 0,3,2 -> 032 gibi)
   strxfrm(x,long_decimal,1);//soldan ba�layarak ikili ikili guruplan�yor ve integer a �evirip diziye yaz�l�yor
   lon=atoi(x);
   //bu//test_[13][1]=lon;
   gsp_loc_buffer[5]=lon;
   memmove(long_decimal,long_decimal+1,9);
   lon=atoi(long_decimal);
   //bu//test_[14][1]=lon;
   gsp_loc_buffer[6]=lon;
   
   sprintf(long_float,"%c%c.%c%c%c%c%c",gps_array[32],gps_array[33], gps_array[35],gps_array[36],gps_array[37],gps_array[38],gps_array[39]); //boylam bilgisinin virg�lden sonras�n� veren 7 digit 
                                                                                                                                             //birle�tirilerek tek bir say� elde edildi ( 1,2,3,4,5,6,7 -> 12.34567 gibi)
   long_float_conv=atof(long_float);  // birle�tirilerek olu�turulan virg�ll� say� float format�na d�n��t�r�ld�
   long_float_conv=long_float_conv/60; // elde edilen float say� 60'a b�l�nerek dakika de�erinin y�zdelik hali elde edildi (59,714/60=0,99523 gibi)
   sprintf(long_dnm,"%.7f",long_float_conv); // float de�i�ken string'e �evirilerek ekrana bas�lmaya uygun hale getirildi
   
   memmove(long_dnm,long_dnm+1,5); strxfrm(x,long_dnm,2); lon=atoi(x);  // k�s�rat k�sm�n�n ba��ndaki 0(s�f�r) ve ,(virg�l)silindi.(0,9646 -> 9646 gibi)  
   //bu//test_[15][1]=lon;        //soldan ba�layarak ikili ikili guruplan�yor ve integer a �evirip diziye yaz�l�yor
   gsp_loc_buffer[7]=lon;
   
   memmove(long_dnm,long_dnm+2,9); strxfrm(x,long_dnm,2); lon=atoi(x); 
   //bu//test_[16][1]=lon;
   gsp_loc_buffer[8]=lon;
   
   memmove(long_dnm,long_dnm+2,9); strxfrm(x,long_dnm,2); lon=atoi(x); 
   //bu//test_[17][1]=lon;
   gsp_loc_buffer[9]=lon;
   
   memmove(long_dnm,long_dnm+2,9); strxfrm(x,long_dnm,2); lon=atoi(x); 
   //bu//test_[18][1]=lon;
   gsp_loc_buffer[10]=lon;
}


void GPS_utc_time(){

   char utc_hour[8],utc_min[8],utc_sec[8];
   char utc_day[8],utc_month[8],utc_year[8];
   unsigned int8 utc;
   
   sprintf(utc_hour,"%c%c",gps_array[4],gps_array[5]);
   sprintf(utc_min,"%c%c",gps_array[6],gps_array[7]);
//!   sprintf(utc_sec,"%c%c",gps_array[8],gps_array[9]);
   utc=atoi(utc_hour);
   gsp_loc_buffer[11]=utc;
   utc=atoi(utc_min);
   gsp_loc_buffer[12]=utc;

   
//!   strncat(utc_min,utc_sec,4);
//!   strncat(utc_hour,utc_min,6);
//!   write_char12_16_Col( 20, 50,utc_hour,RED,1,AMBER,0);
//!   
//!   
//!   sprintf(utc_day,"%c%c",gps_array2[10],gps_array2[11]);
//!   sprintf(utc_month,"%c%c",gps_array2[12],gps_array2[13]);
//!   sprintf(utc_year,"%c%c",gps_array2[14],gps_array2[15]);
//!   
//!   strncat(utc_month,utc_year,4);
//!   strncat(utc_day,utc_month,6);
//!   write_char12_16_Col( 20, 70,utc_day,RED,1,AMBER,0);

}


/*

NEO-6M Data:

$GPGGA,110617.00,41XX.XXXXX,N,00831.54761,W,1,05,2.68,129.0,M,50.1,M,,*42
$GPGSA,A,3,06,09,30,07,23,,,,,,,,4.43,2.68,3.53*02
$GPGSV,3,1,11,02,48,298,24,03,05,101,24,05,17,292,20,06,71,227,30*7C
$GPGSV,3,2,11,07,47,138,33,09,64,044,28,17,01,199,,19,13,214,*7C
$GPGSV,3,3,11,23,29,054,29,29,01,335,,30,29,167,33*4E
(409-414)$GPGLL,41XX.XXXXX,N,00831.54761,W,110617.00,A,A*70
$GPRMC,110618.00,A,41XX.XXXXX,N,00831.54753,W,0.078,,030118,,,A*6A 
$GPVTG,,T,,M,0.043,N,0.080,K,A*2C


1612-UB Data:

$GPRMC,102643.00,A,3959.70217,N,03245.10416,E,0.414,,231019,,,A*7C,<CR>,<LF>   ->>64
$GPVTG,,T,,M,0.414,N,0.767,K,A*24,<CR>,<LF> -->99 
$GPGGA,102643.00,3959.70217,N,03245.10416,E,1,05,6.40,928.4,M,36.2,M,,*5E,<CR>,<LF> -->174
$GPGSA,A,3,22,17,03,01,19,,,,,,,,8.03,6.40,4.85*0E,<CR>,<LF>
$GPGSV,2,1.07,01,81,273,22,03,46,297,28,11,,,09,14,,,08*70,<CR>,<LF>
$GPGSV,2,2,0,7,17,17,315,31,19,<CR>,<LF>
$


*/




