#define RV1805_ADDR 0xd2

#define RV1805_CHIPIDH 0x18
#define RV1805_CHIPIDL 0x05

#define RV1805_CHIPIDH_ADDR 0x28
#define RV1805_CHIPIDL_ADDR 0x29

#define HUNDREDTHS_ADDR  0x00
#define SECONDS_ADDR     0x01
#define MINUTES_ADDR     0x02
#define HOURS_ADDR       0x03
#define DATE_ADDR        0x04
#define MONTH_ADDR       0x05
#define YEAR_ADDR        0x06
#define DAY_ADDR         0x07

#define HUNDREDTHS_ALR   0x08
#define SECONDS_ALR      0x09
#define MINUTES_ALR      0x0A
#define HOURS_ALR        0x0B
#define DATE_ALR         0x0C
#define MONTH_ALR        0x0D
#define DAY_ALR          0x0E

#define PER_MIN          0x18
#define PER_HOUR         0x14
#define PER_DAY          0x10
#define PER_WEEK         0x0C
#define PER_MOUNTH       0x08    
#define PER_YEAR         0x04    

#define CTDWN_TMR_CTRL   0x18



const unsigned int8 timereg[8][2]=   ///time buffer
{

   {HUNDREDTHS_ADDR, 0x00}, {SECONDS_ADDR, 0x00}, {MINUTES_ADDR, 0x1e}, {HOURS_ADDR, 0x0c}, 
   {DATE_ADDR, 0x0f},       {MONTH_ADDR, 0x09},   {YEAR_ADDR, 0x13},    {DAY_ADDR, 0x05}
};

//!int dec_HUNDREDTHS = 0;
//!int dec_SECONDS    = 0;                   
//!int dec_MINUTES    = 0;                   
//!int dec_HOURS      = 0;                   
//!int dec_DATE       = 0;                   
//!int dec_MONTH      = 0;                   
//!int dec_YEAR       = 0;                   
//!int dec_DAY        = 0;                   
//!
//!unsigned int8 hex_HUNDREDTHS = 0x00;
//!unsigned int8 hex_SECONDS    = 0x00;                   
//!unsigned int8 hex_MINUTES    = 0x00;                   
//!unsigned int8 hex_HOURS      = 0x00;                   
//!unsigned int8 hex_DATE       = 0x00;                   
//!unsigned int8 hex_MONTH      = 0x00;                   
//!unsigned int8 hex_YEAR       = 0x00;                   
//!unsigned int8 hex_DAY        = 0x00; 

//////
static int updateTime(void)
{
   I2C_Startt();
   I2C_SendByte( RV1805_ADDR );
   if( !I2C_WaitAck() )
   {
      I2C_Stopp(); 
      return 0;
   }
   I2C_SendByte( 0x00 );
   I2C_WaitAck();
   for (int i=0;i<8;i++)
   {
      I2C_SendByte(timereg[i][1]);
      I2C_WaitAck();
   }
   
   I2C_Stopp();
}

//////////
static int readTime(void)
{
   I2C_Startt();
   I2C_SendByte( RV1805_ADDR );
   if( !I2C_WaitAck() )
   {
      I2C_Stopp(); 
      return 0;
   }
   I2C_SendByte( 0x00 );
   I2C_WaitAck();
   //I2C_Stopp();
   
   I2C_Startt();
   I2C_SendByte( RV1805_ADDR | 0x01 );
   if( !I2C_WaitAck() )
   {
      I2C_Stopp(); 
      return 0;
   }
   
   for(int i=0;i<8;i++){           
      timereg[i][1] = I2C_ReceiveByte();
      I2C_Ack();
   }
   I2C_Stopp();
   
   test_[6][1]=timereg[2][1];//min   
   test_[7][1]=timereg[3][1];//hour  
                                     
   test_[3][1]=timereg[4][1];//day   
   test_[4][1]=timereg[5][1];//mounth
   test_[5][1]=timereg[6][1];//year     
}

//////////
static int setAlarm(unsigned int8 alarm) ///setAlarm(PER_DAY)
{   
   unsigned int8 val;
   
   /*alarm zaman�*/
   I2C_Startt();
   I2C_SendByte( RV1805_ADDR );
   if( !I2C_WaitAck() )
   {
      I2C_Stopp(); 
      return 0;
   }
   I2C_SendByte( 0x08 );
   I2C_WaitAck();
   
   for(int i=0;i<7;i++){
      I2C_SendByte( timereg[i][1] );
   }
   
   I2C_Stopp();
   
   /*alarm ayarlar�*//*alarm �alma s�kl��� ayarlan�yor*/
   I2C_ReadByte( &val, 1, CTDWN_TMR_CTRL, RV1805_ADDR);
   
   val = val & 0xe3;
   val = val | alarm;
   
   I2C_WriteByte( CTDWN_TMR_CTRL, val, RV1805_ADDR);
   
   /*alarm interrupt*//*0x12 register� ba�ka yerde de kullan�lmak istenirse burada d�zenleme yap�lmal�*/ 
   I2C_Startt();
   I2C_SendByte( RV1805_ADDR );
   if( !I2C_WaitAck() )
   {
      I2C_Stopp(); 
      return 0;
   }
   I2C_SendByte( 0x12 );
   I2C_WaitAck();
   I2C_SendByte( 0xE4 );///E8
   I2C_WaitAck();
   I2C_Stopp();
}

//////////
static void setinterrupt(void){

}

//!///////// 
//!static void hextodec(void){
//!   dec_HUNDREDTHS = (timereg[0][1]/0x10)*10 + (timereg[0][1]%0x10);
//!   dec_SECONDS    = (timereg[1][1]/0x10)*10 + (timereg[1][1]%0x10);
//!   dec_MINUTES    = (timereg[2][1]/0x10)*10 + (timereg[2][1]%0x10);
//!   dec_HOURS      = (timereg[3][1]/0x10)*10 + (timereg[2][1]%0x10);
//!   dec_DATE       = (timereg[4][1]/0x10)*10 + (timereg[4][1]%0x10);
//!   dec_MONTH      = (timereg[5][1]/0x10)*10 + (timereg[5][1]%0x10);
//!   dec_YEAR       = (timereg[6][1]/0x10)*10 + (timereg[6][1]%0x10);
//!   dec_DAY        = (timereg[7][1]/0x10)*10 + (timereg[7][1]%0x10);
//!}
//!
//!static void dectohex(void){
//!   hex_HUNDREDTHS = (dec_HUNDREDTHS/10)*0x10 + (dec_HUNDREDTHS%10);
//!   hex_SECONDS    = (dec_SECONDS/10)*0x10 + (dec_SECONDS%10);
//!   hex_MINUTES    = (dec_MINUTES/10)*0x10 + (dec_MINUTES%10);
//!   hex_HOURS      = (dec_HOURS/10)*0x10 + (dec_HOURS%10);
//!   hex_DATE       = (dec_DATE/10)*0x10 + (dec_DATE%10);
//!   hex_MONTH      = (dec_MONTH/10)*0x10 + (dec_MONTH%10);
//!   hex_YEAR       = (dec_YEAR/10)*0x10 + (dec_YEAR%10);
//!   hex_DAY        = (dec_DAY/10)*0x10 + (dec_DAY%10);
//!}

//////////////////////////////////hardware i2c fonksyonlar//////////////////////////////////////////

//!static void updateTime(void)
//!{
//!   i2c_start(RV);
//!   i2c_write(RV, RV1805_ADDR);
//!   i2c_write(RV, 0x00);
//!
//!   for (int i=0;i<8;i++){
//!      i2c_write(RV, timereg[i][1]);
//!   }
//!
//!   i2c_stop(RV);
//!}
//!
//!//////////
//!static void readTime(void)
//!{   
//!   i2c_start(RV);
//!   i2c_write(RV, RV1805_ADDR);
//!   i2c_write(RV, 0x00);
//!   i2c_stop(RV);
//!   i2c_start(RV);
//!   i2c_write(RV, RV1805_ADDR | 0x01);
//!   
//!   for(int i=0;i<8;i++){
//!      timereg[i][1] = i2c_read(RV);
//!   }
//!   
//!   i2c_stop(RV);
//!   
//!   test_[6][1]=timereg[2][1];//min
//!   test_[7][1]=timereg[3][1];//hour
//!   
//!   test_[3][1]=timereg[4][1];//day
//!   test_[4][1]=timereg[5][1];//mounth
//!   test_[5][1]=timereg[6][1];//year
//!}
//!
//!static void setAlarm(unsigned int8 alarm) ///setAlarm(PER_DAY)
//!{   
//!   unsigned int val;
//!   /*alarm zaman�*/
//!   i2c_start(RV);
//!   i2c_write(RV, RV1805_ADDR);
//!   i2c_write(RV, 0x08);
//!   
//!   for(int i=0;i<7;i++){
//!      i2c_write(RV, timereg[i][1]);
//!   }
//!   
//!   i2c_stop(RV);
//!   
//!   /*alarm ayarlar�*//*alarm �alma s�kl��� ayarlan�yor*/
//!   i2c_start(RV);
//!   i2c_write(RV, RV1805_ADDR);
//!   i2c_write(RV, CTDWN_TMR_CTRL);
//!   i2c_stop(RV);
//!   i2c_start(RV);
//!   i2c_write(RV, RV1805_ADDR | 0x01);
//!   val = i2c_read(RV);
//!   
//!   val = val & 0xe3;
//!   val = val | alarm;
//!   
//!   i2c_start(RV);
//!   i2c_write(RV, RV1805_ADDR);
//!   i2c_write(RV, CTDWN_TMR_CTRL);
//!   i2c_write(RV, val);
//!   
//!   /*alarm interrupt*//*0x12 register� ba�ka yerde de kullan�lmak istenirse burada d�zenleme yap�lmal�*/ 
//!   i2c_start(RV);
//!   i2c_write(RV, RV1805_ADDR);
//!   i2c_write(RV, 0x12);
//!   i2c_write(RV, 0xE2);
//!   i2c_stop(RV);
//!}
